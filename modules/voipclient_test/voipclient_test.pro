QT += core
QT += network
QT += gui

CONFIG += c++11

TARGET = voipclient_test
CONFIG += console
CONFIG -= app_bundle

DESTDIR = /app/bin/

TEMPLATE = app

INCLUDEPATH += /app/include/common

#LIBS += -lssh

SOURCES += \
    voipclient_main.cpp \
    voipclient_tcp_client.cpp

HEADERS += \
    voipclient_define.h \
    voipclient_tcp_client.h
