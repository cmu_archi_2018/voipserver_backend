#include "voipclient_tcp_client.h"

CVoIpClientTest::CVoIpClientTest(QString serveraddr,
                                                 quint16 serverport, 
                                                 QObject *parent ) :
    QObject(parent),
    m_sock_(nullptr)
{
    /* Initialize Variables */
    m_server_addr.clear();
    m_server_port   = 0;

    m_readed_data.clear();

    m_server_addr   = serveraddr;
    m_server_port   = serverport;

    m_sock_         = new QTcpSocket(this);

    connect( this, SIGNAL(doConnect()),  this, SLOT(doConnectSlot()));

    emit doConnect();
}

CVoIpClientTest::~CVoIpClientTest()
{
}

void
CVoIpClientTest::connectedslot()
{
    LOG_INFO << "Connected to Server..!!, Waiting for data....";
}

void
CVoIpClientTest::doConnectSlot()
{
    m_sock_->connectToHost(m_server_addr, m_server_port);
    connect(m_sock_, SIGNAL(connected()), this, SLOT(connectedslot()));

    LOG_INFO << "Try to connect to " << m_server_addr << ":" << m_server_port;

    // Waiting for Connection to Server
    while( true == m_sock_->waitForConnected(3000) )  {
        m_readed_data.clear();

        QJsonObject json_obj;
        json_obj["msg"] = "invite";
        json_obj["seqNo"] = 1;
        json_obj["cid"] = "e0b2ff1e-3f4e-49c1-b951-57e149124958";
        json_obj["to"] = "103";
        json_obj["from"] = "102";
        json_obj["protocol"] = "RTP";
        json_obj["codec"] = "GSM";
        json_obj["token"] = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ91.eyJlbWFpbCI6InQzIiwicGhvbmVObyI6MTAyLCJyb2xlIjoiZ2VuZXJhbCIsImlhdCI6MTUyOTQ0NDA1MywiZXhwIjoxNTMwMDQ4ODUzLCJpc3MiOiJ0ZWFtNC5jbXUuZWR1Iiwic3ViIjoidXNlckluZm8ifQ.sYRonC24jPtn9hADuN0878Z_mpvjsKskIbxJMZOP1CQ";

        /*
        "msg":"invite",
        "seqNo":1,
        "cid":"e0b2ff1e-3f4e-49c1-b951-57e149124958",
        "to":"103",
        "from":"102",
        "protocol":"RTP",
        "codec":"GSM",
        "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InQzIiwicGhvbmVObyI6MTAyLCJyb2xlIjoiZ2VuZXJhbCIsImlhdCI6MTUyOTQ0NDA1MywiZXhwIjoxNTMwMDQ4ODUzLCJpc3MiOiJ0ZWFtNC5jbXUuZWR1Iiwic3ViIjoidXNlckluZm8ifQ.sYRonC24jPtn9hADuN0878Z_mpvjsKskIbxJMZOP1CQ"
        */

        QJsonDocument json_doc(json_obj);
        QString json_string = json_doc.toJson();

        LOG_DEBUG << "B _ json_string : " << json_string;
        json_string = json_string.simplified();
        json_string = json_string.remove(QRegExp("[\r\n]"));
        json_string.append("\r\n");
        LOG_DEBUG << "A _ json_string : " << json_string;

        m_sock_->write(json_string.toUtf8());

        // Waiting for readable data in socket buffer
        if( true == m_sock_->waitForReadyRead() ) {
            m_readed_data = m_sock_->readAll();

            LOG_INFO << "Recevied Data is " << m_readed_data.toStdString().c_str();

//            QTextStream in(m_readed_data);

//            if( false == m_ssh_connected ) {
//                emit sshConnect();
//            } // if
//            else {
//                // Parsing receviced data from Server
//                QString l = in.readLine();
//                LOG_VERB << "file : " << l;
//                while( false == l.isEmpty() ) {
//                    // Add Command
//                    if( MATCHED == l.compare("add") ) {
//                        l = in.readLine();
//                        if( false == l.isEmpty() ) downloadFiles(l);
//                    }
//                    // Remove Command
//                    else if( MATCHED == l.compare("remove") ) {
//                        l = in.readLine();
//                        if( false == l.isEmpty() ) {
//                            QList<QString> f = l.split('/');
//                            QFile file;
//                            file.setFileName( m_file_path + '/' + f.back() );
//                            file.remove();
//                        }
//                    } // if(
//                    l = in.readLine();
//                } // while
//            } // else
            break;
        } // if

        m_readed_data.clear();
    }
    LOG_INFO << " Disconnected from Server..!! Try to connect after 100ms...";
    // If connection to server was disconnected, we will retry to connect again after 1000 ms.
//    QTimer::singleShot(1000, this, SLOT(doConnectSlot()));
}
