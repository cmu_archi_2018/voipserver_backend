#include <QCoreApplication>
#include <voipclient_define.h>
#include <voipclient_tcp_client.h>

#include <iostream>

void print_help()
{
    std::cout << " Usage : syncimagefiles [configfile]" << std::endl;
    std::cout << " [configfile] : default file is on /app/config/config_syncimagefiles.json" << std::endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString configfile;
    if( argc != 2 ) {
        print_help();
        configfile = "/app/config/config_syncimagefiles.json";
    }
    else {
        configfile = argv[1];
    }

//    LOG_INIT("/app/config","/app/log","syncimagefiles","","","");

    LOG_INFO << "********************************************************" ;
    LOG_INFO << "***          VoIpClient_Test is Started               ***" ;
    LOG_INFO << "***          Process ID = " << a.applicationPid() << "                     ***" ;
    LOG_INFO << "********************************************************" ;

    QFile   qconfig(configfile);
    qconfig.open( QIODevice::ReadOnly );

    if( false == qconfig.isOpen() ) {
        LOG_ERROR << "configfile(" << configfile<< ") could NOT open..!!";
        exit(0);
    }

    QByteArray  arr;
    arr = qconfig.readAll();

    QJsonObject obj    = QJsonDocument::fromJson(arr.toStdString().c_str()).object();
    QString function   = obj.value("function").toString();
    QString serveraddr = obj.value("serveraddr").toString();
    quint16 serverport = obj.value("serverport").toInt();
//    quint16 sshport    = obj.value("sshport").toInt();
//    QString username   = obj.value("username").toString();
//    QString password   = obj.value("password").toString();

    if( MATCHED == function.toUpper().compare("CLIENT") ) { // MATCHED CLIENT FUNCTION

        LOG_INFO << " - function   : " << function.toUpper();
        LOG_INFO << " - serveraddr : " << serveraddr;
        LOG_INFO << " - serverport : " << serverport;
//        LOG_INFO << " - sshport    : " << sshport;
//        LOG_INFO << " - username   : " << username;
//        LOG_INFO << " - password   : " << password;

        QString server_addr(serveraddr);
        //new CVoIpClientTest(directory, server_addr, serverport, username, password, sshport);
        new CVoIpClientTest(server_addr, serverport);
    }
//    else if( MATCHED == function.toUpper().compare("SERVER") ) { // MATCHED SERVER FUNCTION

//        LOG_INFO << " - function   : " << function.toUpper();
//        LOG_INFO << " - serverport : " << serverport;
//        LOG_INFO << " - directory  : " << directory;

//        new CSyncImageFileTcpServer(directory, serverport);
//    }
    else {
        LOG_ERROR << "Configuration file has problem, please check it..!!!";
        exit(0);
    }

    return a.exec();
}
