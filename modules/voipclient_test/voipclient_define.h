#ifndef SYNCIMAGEFILES_DEFINE_H
#define SYNCIMAGEFILES_DEFINE_H

#include <QObject>
#include <QThread>
#include <QtNetwork>
#include <QTcpSocket>

#include <memory>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

#include <QDebug>
#include <memory>

const quint16           NOT_CONNECTED = 0;
const quint16           MATCHED = 0;

#define LOG_INFO    qDebug() << "[I]"
#define LOG_WARN    qDebug() << "[W]"
#define LOG_ERROR   qDebug() << "[E]"
#define LOG_DEBUG   qDebug() << "[D]"

#endif // SYNCIMAGEFILES_DEFINE_H
