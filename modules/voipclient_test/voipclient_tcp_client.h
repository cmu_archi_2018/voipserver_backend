#ifndef CVoIpClientTest_H
#define CVoIpClientTest_H

#include <QObject>
#include <voipclient_define.h>

//class QSshSocket;

class CVoIpClientTest : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpClientTest( QString /*serveraddr*/,
                              quint16 /*serverport*/,
                              QObject *parent = 0);
    ~CVoIpClientTest();

private:
    /* const */

    QTcpSocket		        *m_sock_;

    QString		            m_server_addr;
    quint32		            m_server_port;

    QByteArray		        m_readed_data;

signals:
    void                    doConnect();

public slots:
    void		            doConnectSlot();

    // Called by other class
    void                    connectedslot();
};

#endif // CVoIpClientTest_H
