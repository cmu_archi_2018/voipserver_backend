QT += core network
QT -= gui

CONFIG += c++14

FLAGS += -Wunused_parameter

TARGET = voipserver_main
CONFIG += console
CONFIG -= app_bundle

DESTDIR = /app/bin

TEMPLATE = app

#GTEST = /usr/src/gtest/src/gtest-all.cc /usr/src/gtest/src/gtest_main.cc

INCLUDEPATH += ./hdr
INCLUDEPATH += /usr/include/glib-2.0
INCLUDEPATH += /usr/include/gstreamer-1.0
INCLUDEPATH += /usr/lib/glib-2.0/include
INCLUDEPATH += /usr/lib/x86_64-linux-gnu/glib-2.0/include
INCLUDEPATH += /usr/lib/x86_64-linux-gnu/gstreamer-1.0/include
INCLUDEPATH += /usr/local/include/mongocxx/v_noabi/
INCLUDEPATH += /usr/local/include/bsoncxx/v_noabi/

LIBS += -lpthread -lglib-2.0 -lgobject-2.0 -lgstreamer-1.0
LIBS += -lssl -lcrypto
LIBS += -L/usr/local/lib -lbsoncxx -lmongocxx
#LIBS += -lgstallocators-1.0 -lgstapp-1.0

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
#    voipserver_call.h \
#    voipserver_text.h \
    hdr/voipserver_define.h \
    hdr/voipserver_config.h \
#    voipserver_gstreamer.h \
#    voipserver_gstreamer_loop.h \
    hdr/voipserver_database.h \
    hdr/voipserver_client_worker.h \
#    voipserver_keepalive_server.h \
    hdr/voipserver_init.h \
    hdr/voipserver_listen_server.h \
    hdr/jwt/detail/meta.hpp \
    hdr/jwt/json/json.hpp \
    hdr/jwt/algorithm.hpp \
    hdr/jwt/base64.hpp \
    hdr/jwt/error_codes.hpp \
    hdr/jwt/exceptions.hpp \
    hdr/jwt/jwt.hpp \
    hdr/jwt/parameters.hpp \
    hdr/jwt/short_string.hpp \
    hdr/jwt/stack_alloc.hpp \
    hdr/jwt/string_view.hpp \
    hdr/jwt/impl/algorithm.ipp \
    hdr/jwt/impl/error_codes.ipp \
    hdr/jwt/impl/jwt.ipp \
    hdr/jwt/impl/stack_alloc.ipp \
    hdr/jwt/impl/string_view.ipp \
    hdr/voipserver_confcall.h \
    hdr/voipserver_confcall_worker.h

#    hdr/voipserver_call.h \
#    hdr/voipserver_gstreamer.h \
#    hdr/voipserver_gstreamer_loop.h \
#    hdr/voipserver_keepalive_server.h \
#    hdr/voipserver_text.h \

SOURCES += \
    src/voipserver_main.cpp \
#    voipserver_call.cpp \
#    voipserver_text.cpp \
    src/voipserver_config.cpp \
#    voipserver_gstreamer.cpp \
#    voipserver_gstreamer_loop.cpp \
    src/voipserver_database.cpp \
    src/voipserver_client_worker.cpp \
#    voipserver_keepalive_server.cpp \
    src/voipserver_init.cpp \
    src/voipserver_listen_server.cpp \
    src/voipserver_confcall.cpp \
    src/voipserver_confcall_worker.cpp
