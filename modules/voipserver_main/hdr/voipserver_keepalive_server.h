#ifndef VOIPSERVER_KEEPALIVE_SERVER_H
#define VOIPSERVER_KEEPALIVE_SERVER_H

#include "voipserver_define.h"
#include "voipserver_client_worker.h"
#include "voipserver_database.h"

#include <QTcpServer>

class CVoIpServerClientWorker;
class CVoIpServerDataBase;
extern CVoIpServerConfig g_config;

class CVoIpServerKeepAliveServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit CVoIpServerKeepAliveServer(quint16 /*keepaliveport*/, QObject *parent = 0);
    virtual ~CVoIpServerKeepAliveServer();

    void                            start_server(quint16 /*port*/);

private:
    quint16                         m_server_port;
    std::map<QString, \
          CVoIpServerClientWorker*> g_map_of_conn;

    CVoIpServerDataBase*            m_database_;

signals:

public slots:
    void                            slot_disconnected(CVoIpServerClientWorker* /*client_thread*/);

protected :
    // if new connection was mode
    void                            incomingConnection(qintptr /*sock_fd*/) override final;
};

#endif // VOIPSERVER_KEEPALIVE_SERVER_H
