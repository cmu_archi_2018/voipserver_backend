#ifndef VOIPSERVER_DATABASE_H
#define VOIPSERVER_DATABASE_H

#include "voipserver_define.h"
#include "voipserver_config.h"

#include <iostream>

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/exception/exception.hpp>

namespace mdb {
enum DB_RESULT_E{
    NONE            = 0,
    DONE            = 1,
    FAILED,
    NUMBER_VALID,
    NUMBER_NOT_VALID,
    NUMBER_CONFERENCE,
    NUMBER_NOT_ONLINE,
    NUMBER_IN_CALLING,
    TEXT_CACHED,
    TEXT_MULTI_CACHED,
    USER_VALID_OF_CONFCALL,
    USER_NOT_VALID_OF_CONFCALL,
    DATABASE_NOT_UPDATED,
    NO_DATA_IN_DATABASE,
    NOT_VALID_CONFCALL
};

struct DB_CACHED_TEXT {
    QString     caller;
    QString     callee;
    QString     msg;
};
}

using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;

using namespace std;
using namespace mdb;

extern CVoIpServerConfig g_config;

class CVoIpServerDataBase : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerDataBase(QObject *parent = 0);


    DB_RESULT_E        /**/ get_user_address_from_directories(QString/*targetNumber*/, QString& /*OUT addr*/);
    DB_RESULT_E        /**/ get_user_info_from_users(QString/*userNo*/,QString&/*email*/);

    DB_RESULT_E        /**/ set_update_keepalive_standby(QString /*phonenumber*/, QString /*ipaddr*/, QList<DB_CACHED_TEXT>& /*cached_text*/);
    DB_RESULT_E        /**/ set_update_keepalive_calling(QString /*phonenumber*/, QString /*ipaddr*/);

    DB_RESULT_E        /**/ set_insert_text_message(QString /*caller*/, QString /*caller_email*/, QString /*callee*/, QString /*callee_email*/, QString /*msg*/, bool /*is_complete*/);
    DB_RESULT_E        /**/ set_complete_all_text_message(QString /*callee*/);

    DB_RESULT_E        /**/ set_request_call    (QString /*cid*/, QString /*callerNo*/, QString /*callerEmail*/, QDateTime /*request_datetime*/);
    DB_RESULT_E        /**/ set_start_call      (QString /*cid*/, QString /*callerNo*/, QString /*calleeNo*/, QString /*callerEmail*/, QString /*calleeEmail*/, QDateTime /*start_datetime*/);
    DB_RESULT_E        /**/ set_update_call     (QString /*cid*/, QDateTime/*start_datetime*/);
    DB_RESULT_E        /**/ set_end_call        (QString /*cid*/, QDateTime/*end_dateTime*/);

    DB_RESULT_E             get_valid_user_of_conf_call(QString /*phonenumber*/, QString /*confcallnumber*/);
    QString            /**/ get_mdb_error_to_string(mdb::DB_RESULT_E /*msg*/);

private:
//    QNetworkAccessManager*      m_network_manager;
    double                 m_charge_of_minute;
    double                 m_charge_of_message;

signals:

public slots:

};

#endif // VOIPSERVER_DATABASE_H
