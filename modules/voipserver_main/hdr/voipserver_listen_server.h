#ifndef VOIPSERVER_TCPSOCKET_H
#define VOIPSERVER_TCPSOCKET_H

#include "voipserver_define.h"
#include "voipserver_client_worker.h"
#include "voipserver_database.h"
#include "voipserver_confcall.h"

#include <QTcpServer>

class CVoIpServerClientWorker;
class CVoIpServerDataBase;
class CVoIpServerConfCall;

extern CVoIpServerConfig g_config;


class CVoIpServerListenServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit CVoIpServerListenServer(quint16/*serverport*/,QObject *parent = 0);
    virtual ~CVoIpServerListenServer();

    void                            start_server(quint16 /*port*/);
    bool                            set_database(CVoIpServerDataBase* /*database_*/);
    bool                            set_confcall(CVoIpServerConfCall* /*confcall_*/);

    void                            set_keepalive();

private:
    quint16                         m_server_port;
    bool                            m_is_keepalive;

    std::map<QString, \
          CVoIpServerClientWorker*> g_map_of_conn;

    CVoIpServerDataBase*            m_database_;
    CVoIpServerConfCall*            m_confcall_;

signals:

public slots:
    void                            slot_disconnected(CVoIpServerClientWorker* /*client_thread*/);

protected :
    // if new connection was mode
    void                            incomingConnection(qintptr /*sock_fd*/) override final;
};

#endif // VOIPSERVER_TCPSOCKET_H
