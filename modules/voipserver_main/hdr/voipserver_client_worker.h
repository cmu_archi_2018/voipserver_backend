#ifndef VOIPSERVER_TCPTHREAD_H
#define VOIPSERVER_TCPTHREAD_H

#include "voipserver_define.h"
#include "voipserver_database.h"
#include "voipserver_confcall.h"

#include <iostream>
#include "jwt/jwt.hpp"

extern CVoIpServerConfig    g_config;

using namespace std;
using namespace mdb;

class CVoIPServerCall;
class CVoIPServerText;
class CVoIpServerDataBase;
class CVoIpServerConfCall;

class CVoIpServerClientWorker : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerClientWorker(qintptr /*fd*/, QObject *parent = 0);
    ~CVoIpServerClientWorker();

    QString                 parsing_addr(QString /*clientaddr*/);
    bool                    set_database(CVoIpServerDataBase* /*pointer*/);
    bool                    set_confcall(CVoIpServerConfCall* /*object*/);

    QString                 m_caller_phone;
    QString                 m_callee_phone;

private:
    /*
     * For Call History
     * every client worker should have caller/callee information
     * & request date or start date / end date
     */
    QString                 m_caller_email;
    QString                 m_callee_email;

    QDateTime               m_call_request_date;
    QDateTime               m_call_start_date;
    QDateTime               m_call_end_date;
    /**************************************************************/


    // This is indicated whether this class is for Conference call or not.
    bool                    m_is_conf_call;
    /**************************************************************/

    /*
     * For Connection
     */
    QSslSocket*             m_caller_sock_;
    qintptr 	            m_caller_sock_fd;
    QString                 m_caller_addr;

    //QSslSocket*             m_callee_sock_;
    QTcpSocket*             m_callee_sock_;
    qintptr 	            m_callee_sock_fd;
    QString                 m_callee_addr;
    /**************************************************************/

    QProcess*               m_ringback_process;

    // Trim Json String
    QString                 simplified_json_string(QString /*json str*/);

    CVoIpServerDataBase*    m_database_;
    CVoIpServerConfCall*    m_confcall_;

    /*
     * for validating token
     */
    bool                    get_valid_token(QString/*token*/);

    /*
     * For SSL Communication
     * Below part is related SSL Communication
     */
    QSslCertificate         &get_ssl_local_certificate();
    QSslKey                 &get_ssl_private_key();
    QSsl::SslProtocol       get_ssl_protocol();

    void                    set_ssl_local_certificate(const QSslCertificate &certificate);
    bool                    set_ssl_local_certificate(const QString &/*path*/, QSsl::EncodingFormat format = QSsl::Pem);

    void                    set_ssl_private_key(const QSslKey &/*key*/);
    bool                    set_ssl_private_key(const QString &/*filename*/, QSsl::KeyAlgorithm algorithm = QSsl::Rsa, QSsl::EncodingFormat  format = QSsl::Pem, const QByteArray &passPhrase = QByteArray());

    void                    set_ssl_protocol(QSsl::SslProtocol /*protocol*/);

    QSslCertificate         m_ssl_local_certificate;
    QSslKey                 m_ssl_private_key;
    QSsl::SslProtocol       m_ssl_protocol;
    /**************************************************************/

signals:
    void                    signal_error( QTcpSocket::SocketError/*socketerror*/);

    // to TcpServer for managing connection information
    void                    signal_disconnecting( CVoIpServerClientWorker* );

    void                    signal_stop_ringback();

public slots:
    void                    slot_do_work();
    void                    slot_read_of_caller();
    void                    slot_read_of_callee();
    void                    slot_disconnect_of_caller();
    void                    slot_disconnect_of_callee();
    void                    slot_ready_of_caller();
    void                    slot_ready_of_callee();
};

#endif // VOIPSERVER_TCPTHREAD_H
