#ifndef VOIPSERVER_CONFCALL_WORKER_H
#define VOIPSERVER_CONFCALL_WORKER_H

#include "voipserver_define.h"
#include "voipserver_config.h"

extern CVoIpServerConfig g_config;

class CVoIpServerConfCallWorker : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerConfCallWorker(QString /*number*/, QObject *parent = 0);

    bool                    set_start_port_num(const quint16 /*start_ports_num*/);

    bool                    set_add_user(QString /*addr*/, quint16 /*port*/);
    bool                    set_del_user(QString /*addr*/);

    quint16                 get_available_port();

    QString                 m_confcall_number;
    bool                    m_expired;

private:
    const quint16           MAX_USER_OF_CONF_CALL       = 4;

    const bool              NOT_USED        = false;
    const bool              USED            = true;

    const quint16           PORT_NUM_ROW    = 0;
    const quint16           PORT_CHECK_ROW  = 1;

    QVector<QVector<quint16>>                           m_port_vector;

    QHash<quint16, bool>                                m_hash_of_port_res;

    QHash<quint16,QList<quint16>>                       m_hash_of_src_ports;
    QHash<quint16,QList<quint16>>                       m_hash_of_sink_ports;

    QList<QPair<quint16,QProcess*>>                     m_list_of_src_proc;
    QHash<QString,QPair<quint16,QProcess*>>             m_hash_of_sink_proc;

signals:
    void                    signal_destory_me(QString /*confcallnumber*/);

public slots:
};

#endif // VOIPSERVER_CONFCALL_WORKER_H
