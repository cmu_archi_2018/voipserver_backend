#ifndef VOIPSERVER_CONFIG_H
#define VOIPSERVER_CONFIG_H

#include "voipserver_define.h"

class CVoIpServerConfig {
public :
    explicit    CVoIpServerConfig();
    bool        set_config(QString filepath);

    QString     m_config_file;
    QString     m_function;

    QString     m_server_addr;
    quint16     m_server_port;
    quint16     m_client_signal_port;
    quint16     m_client_media_port;

    quint16     m_message_duration;

    QString     m_database_uri;
    QString     m_test_callee_addr;

    QString     m_ssl_certificate;
    QString     m_ssl_key;
    qint16      m_ssl_protocol;

    quint16     m_keepalive_port;
    quint32     m_keepalive_interval;
    quint16     m_keepalive_missing;

    QString     m_token_secret_key;
    QString     m_token_issuer;

    QString     m_ringback_bin;
    QString     m_confcall_bin;

    quint16     m_confcall_start_port;

    enum {
        LOG_LEVEL_DEBUG = 0,
        LOG_LEVEL_WARN  = 1,
        LOG_LEVEL_ERROR = 2,
        LOG_LEVEL_MAX
    }LOG_LEVEL_E;

    QBitArray   m_log_level;

private :
    quint16     convert_ssl_protocol(QString protocol);
};

#endif // VOIPSERVER_CONFIG_H
