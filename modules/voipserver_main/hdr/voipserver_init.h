#ifndef VOIPSERVER_INIT_H
#define VOIPSERVER_INIT_H

#include "voipserver_define.h"
#include "voipserver_config.h"
#include "voipserver_listen_server.h"
#include "voipserver_keepalive_server.h"
#include "voipserver_database.h"
#include "voipserver_confcall.h"

extern CVoIpServerConfig g_config;

class CVoIpServerListenServer;
class CVoIpServerDataBase;
class CVoIpServerConfCall;

class CVoIpServerInit : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerInit(QObject *parent = 0);

private:
    CVoIpServerListenServer*           m_command_server_;
    CVoIpServerListenServer*           m_keepalive_server_;
    CVoIpServerDataBase*               m_database_;
    CVoIpServerConfCall*               m_confcall_;

    void                               init();

signals:

public slots:
};

#endif // VOIPSERVER_INIT_H
