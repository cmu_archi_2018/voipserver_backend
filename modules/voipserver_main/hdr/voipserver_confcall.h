#ifndef VOIPSERVER_CONFCALL_H
#define VOIPSERVER_CONFCALL_H

#include "voipserver_define.h"
#include "voipserver_config.h"
#include "voipserver_database.h"
#include "voipserver_confcall_worker.h"

extern CVoIpServerConfig g_config;

class CVoIpServerDataBase;
class CVoIpServerConfCallWorker;

class CVoIpServerConfCall : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerConfCall(QObject *parent = 0);

    bool        set_database(CVoIpServerDataBase* /*database_*/);

    quint16     ask_to_join(QString /*conf_number*/, QJsonObject /*jsonobj*/);

    bool        join_confcall(QString /*conf_number*/, QString /*cliendAddr*/, quint16 /*port*/);
    bool        exit_confcall(QString /*conf_number*/, QString /*cliendAddr*/, quint16 /*port*/);

    quint16     get_available_port_of_confcall_room(QString /*conf_number*/);
    bool        destory_confcall_room(QString/*conf_number*/);

private :

    CVoIpServerDataBase*                             m_database_;

    QHash<QString,CVoIpServerConfCallWorker*>        m_hash_of_conf_call;
    QHash<quint16,bool>                              m_hash_of_port_res;

    const bool              NOT_USED        = false;
    const bool              USED            = true;


    const quint16           MAX_CONFCALL_ROOM       = 50;
    const quint16           MAX_USED_PORT_PER_ROOM  = 20;

    quint16                 get_available_port();

signals:

public slots:
    void                    slot_destroy_worker(QString /*confcall*/);
};

#endif // VOIPSERVER_CONFERENCECALL_H
