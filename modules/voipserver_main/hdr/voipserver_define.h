#ifndef VOIPSERVER_DEFINE_H
#define VOIPSERVER_DEFINE_H

#include <QObject>
#include <QThread>
#include <QtNetwork>
#include <QTcpSocket>
#include <QString>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

#include <QDebug>
#include <QMutex>
#include <memory>

#include <gst/gst.h>
#include <gst/gstbuffer.h>
#include <pthread.h>

#include "voipserver_config.h"

const quint16           NOT_CONNECTED = 0;
const quint16           MATCHED = 0;


#ifndef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

#define LOG_PRE     qDebug().noquote() << QString("%1, [%2, %3]").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz")).arg(__FILENAME__, -40).arg(__LINE__, 5)

#define LOG_INFO    LOG_PRE << " : [I]"
#define LOG_WARN    LOG_PRE << " : [W]"
#define LOG_ERROR   LOG_PRE << " : [E]"
#define LOG_DEBUG   LOG_PRE << " : [D]"

#endif // VOIPSERVER_DEFINE_H
