#include "voipserver_confcall_worker.h"

CVoIpServerConfCallWorker::CVoIpServerConfCallWorker(QString number, QObject *parent) : QObject(parent)
{
    m_confcall_number = number;
}

bool
CVoIpServerConfCallWorker::set_start_port_num(const quint16 start_port)
{
    /*
    QVector<quint16> ports;
    ports.clear();

    for( quint16 idx = 0 ; idx < MAX_USER_OF_CONF_CALL ; idx++ ) {
        ports.push_back(start_port+idx);
    }

    m_port_vector.push_back(ports);
    ports.clear();
    for( quint16 idx = 0 ; idx < MAX_USER_OF_CONF_CALL ; idx++ ) {
        ports.push_back(NOT_USED);
    }
    */

    for( quint16 idx = 0 ; idx < MAX_USER_OF_CONF_CALL ; idx++ ) {
        // RTP Protocol use Even Number port for media
        m_hash_of_port_res.insert(start_port+(2*idx), NOT_USED);
        LOG_DEBUG << " port number : " << start_port+idx << " was inserted..!!";
    }

    LOG_DEBUG << " m_hash_of_port_res size : " << m_hash_of_port_res.size();

    QHash<quint16,bool>::iterator           iter_ports;
    iter_ports = m_hash_of_port_res.begin();

    /*
     * Setting source Pipeline
     */
    QList<quint16>  temp_list;
    temp_list.push_back(start_port + 10); //4

    if( iter_ports != m_hash_of_port_res.end() )
        m_hash_of_src_ports.insert(iter_ports.key(), temp_list);

    temp_list.clear(); iter_ports++;

    temp_list.push_back(start_port + 12); //5

    if( iter_ports != m_hash_of_port_res.end() )
        m_hash_of_src_ports.insert(iter_ports.key(), temp_list);

    temp_list.clear(); iter_ports++;

    temp_list.push_back(start_port + 14); //6

    if( iter_ports != m_hash_of_port_res.end() )
        m_hash_of_src_ports.insert(iter_ports.key(), temp_list);

    temp_list.clear(); iter_ports++;

    temp_list.push_back(start_port + 16); //7

    if( iter_ports != m_hash_of_port_res.end() )
        m_hash_of_src_ports.insert(iter_ports.key(), temp_list);

    /*
     * Setting Sink Pipeline
     */
    temp_list.clear();
    iter_ports = m_hash_of_port_res.begin();

    temp_list.push_back(start_port + 12 );//5);
    temp_list.push_back(start_port + 14 );//6);
    temp_list.push_back(start_port + 16 );//7);

    /* for User 1 */
    if( iter_ports != m_hash_of_port_res.end() )
        m_hash_of_sink_ports.insert(iter_ports.key(), temp_list);

    temp_list.clear(); iter_ports++;

    temp_list.push_back(start_port + 10 );//4);
    temp_list.push_back(start_port + 14 );//6);
    temp_list.push_back(start_port + 16 );//7);

    /* for User 2 */
    if( iter_ports != m_hash_of_port_res.end() )
        m_hash_of_sink_ports.insert(iter_ports.key(), temp_list);

    temp_list.clear(); iter_ports++;

    temp_list.push_back(start_port + 10 );//4);
    temp_list.push_back(start_port + 12 );//5);
    temp_list.push_back(start_port + 16 );//7);

    /* for User 3 */
    if( iter_ports != m_hash_of_port_res.end() )
        m_hash_of_sink_ports.insert(iter_ports.key(), temp_list);

    temp_list.clear(); iter_ports++;

    temp_list.push_back(start_port + 10 );//4);
    temp_list.push_back(start_port + 12 );//5);
    temp_list.push_back(start_port + 14 );//6);

    /* for User 4 */
    if( iter_ports != m_hash_of_port_res.end() )
        m_hash_of_sink_ports.insert(iter_ports.key(), temp_list);

    QHash<quint16,QList<quint16>>::iterator         iter;
    QList<quint16>  src_list;
    for( iter_ports = m_hash_of_port_res.begin(); iter_ports != m_hash_of_port_res.end(); iter_ports++ ) {
        /* Make Src Pipeline */
        iter = m_hash_of_src_ports.find(iter_ports.key());

        if( iter != m_hash_of_src_ports.end() && iter.key() == iter_ports.key() ) {
            src_list = iter.value();
        }

        QProcess        *t_src_p_ = new QProcess();
        QStringList     arguments;
        QString         confcall_bin = g_config.m_confcall_bin;

        arguments << g_config.m_config_file << "src" << m_confcall_number << QString::number(0) << QString::number(iter_ports.key());
        for( quint16 p : src_list ) {
            LOG_DEBUG << " ports in src_list => " << p;
            arguments << QString::number(p);
        }

        QThread *th_ = new QThread();
        t_src_p_->moveToThread(th_);

        LOG_DEBUG << confcall_bin << arguments;
        t_src_p_->start(confcall_bin, arguments);


        if( false == t_src_p_->waitForStarted(300)) {
            LOG_ERROR << " Could NOT start SRC Process..!!";
            return false;
        }

        QPair<quint16, QProcess*>   t_pair;
        t_pair.first    = iter_ports.key();
        t_pair.second   = t_src_p_;
        m_list_of_src_proc.append(t_pair);
    }

    return true;
}

bool
CVoIpServerConfCallWorker::set_add_user(QString clientaddr, quint16 port)
{
    (void)port;
    QHash<quint16,QList<quint16>>::iterator         iter;
    QHash<quint16,bool>::iterator                   iter_ports;

//    QList<quint16>  src_list;  src_list.clear();
    QList<quint16>  sink_list; sink_list.clear();

    for( iter_ports = m_hash_of_port_res.begin(); iter_ports != m_hash_of_port_res.end(); iter_ports++ ) {
        if( NOT_USED == iter_ports.value() ) {
            /*
             * Create Sink Pipeline
             */
            iter = m_hash_of_sink_ports.find(iter_ports.key());

            if( iter != m_hash_of_sink_ports.end() && iter.key() == iter_ports.key() ) {
                sink_list = iter.value();
            }

            QStringList     arguments;
            QString         confcall_bin = g_config.m_confcall_bin;
#if 0
            /* Make Src Pipeline */
            iter = m_hash_of_src_ports.find(iter_ports.key());

            if( iter != m_hash_of_src_ports.end() && iter.key() == iter_ports.key() ) {
                src_list = iter.value();
            }

            QProcess        *t_src_p_ = new QProcess();

            arguments << g_config.m_config_file << "src" << clientaddr << QString::number(port) << QString::number(iter_ports.key());
            for( quint16 p : src_list ) {
                LOG_DEBUG << " ports in src_list => " << p;
                arguments << QString::number(p);
            }

//            arguments << "&>" << QString("\\tmp\\%1_confcallsrc.log").arg(iter_ports.key());

            LOG_DEBUG << confcall_bin << arguments;
            t_src_p_->start(confcall_bin, arguments);

            if( false == t_src_p_->waitForStarted(300)) {
                LOG_ERROR << " Could NOT start SRC Process..!!";
                return false;
            }

            iter_ports.value() = USED;

            LOG_DEBUG << " src process START..!! port : " << iter_ports.key() << " Process : " << t_src_p_;
#endif
            arguments.clear();

            QProcess        *t_sink_p_ = new QProcess();

            arguments << g_config.m_config_file << "sink" << clientaddr << QString::number(port) << QString::number(iter_ports.key());
            for( quint16 p : sink_list ) {
                LOG_DEBUG << " ports in sink_list => " << p;
                arguments << QString::number(p);
            }
//            arguments << "&>" << QString("\\tmp\\%1_confcallsink.log").arg(iter_ports.key());

            LOG_DEBUG << confcall_bin << arguments;
            QThread *th_ = new QThread();
            t_sink_p_->moveToThread(th_);

            t_sink_p_->start(confcall_bin, arguments);

            if(false == t_sink_p_->waitForStarted(300)) {
                LOG_ERROR << " Could NOT start SINK Process..!!";
                /*
                 * Rollback from m_hash_of_src_proc
                 */
                /*
                QHash<QString,QPair<quint16, QProcess*>>::iterator  t_iter;

                t_iter  = m_hash_of_src_proc.find(clientaddr);

                if( t_iter != m_hash_of_src_proc.end() ) {
                    t_pair    = t_iter.value();
                    if( t_pair.first == iter_ports.key() ) {
                        iter_ports.value() = NOT_USED;
                    }

                    t_pair.second->terminate();
                    m_hash_of_src_proc.remove(clientaddr);
                }
                */

                return false;
            }

            QPair<quint16,QProcess*>    t_pair;
            t_pair.first  = iter_ports.key();
            t_pair.second = t_sink_p_;

            iter_ports.value() = USED;
            LOG_DEBUG << " sink process START..!! port : " << iter_ports.key() << " Process : " << t_sink_p_;

            m_hash_of_sink_proc.insert(clientaddr, t_pair);

            return true;
        }
    }
    return true;
}

bool
CVoIpServerConfCallWorker::set_del_user(QString clientaddr)
{
    QHash<QString,QPair<quint16,QProcess*>>::iterator           iter;
    QHash<quint16,bool>::iterator                               iter_ports;

//    iter = m_hash_of_src_proc.find(clientaddr);
//    if( iter != m_hash_of_src_proc.end() ) {
//        QPair<quint16,QProcess*>  pair = iter.value();

//        quint16     port    = pair.first;
//        QProcess*   process = pair.second;

//        LOG_DEBUG << " src process STOP..!! port : " << port << " Process : " << process;

//        process->terminate();

//        iter_ports = m_hash_of_port_res.find(port);

//        if( iter_ports != m_hash_of_port_res.end() ) {
//            LOG_DEBUG << " port status was changed as NOT_USED in port source map";
//            iter_ports.value() = NOT_USED;
//        }
//    }

    iter = m_hash_of_sink_proc.find(clientaddr);
    if( iter != m_hash_of_sink_proc.end() ) {
        QPair<quint16,QProcess*>   pair = iter.value();

        quint16     port    = pair.first;
        QProcess*   process = pair.second;

        LOG_DEBUG << " sink process STOP..!! port : " << port << " Process : " << process;

        if( nullptr != process )process->terminate();

        LOG_DEBUG << " m_hash_of_port_res : " << m_hash_of_port_res << ", size : " << m_hash_of_port_res.size();
        iter_ports = m_hash_of_port_res.find(port);

        if( iter_ports != m_hash_of_port_res.end() ) {
            LOG_DEBUG << " port status was changed as NOT_USED in port source map";
            iter_ports.value() = NOT_USED;
        }
        m_hash_of_sink_proc.remove(iter.key());
    }

    if( 0 == m_hash_of_sink_proc.size() ) { // there is no users in this conference room
        LOG_WARN << " There is no users in this conference room : " << m_confcall_number;
        for( quint16 idx = 0 ; idx < m_list_of_src_proc.size() ; idx++ ) {
            QPair<quint16,QProcess*>   pair = m_list_of_src_proc.value(idx);

            quint16     port    = pair.first;
            QProcess*   process = pair.second;

            LOG_DEBUG << " src process STOP..!! port : " << port << " Process : " << process;

            if( nullptr != process ) process->terminate() ;

            LOG_DEBUG << " m_hash_of_port_res : " << m_hash_of_port_res << ", size : " << m_hash_of_port_res.size();
            iter_ports = m_hash_of_port_res.find(port);
            if( iter_ports != m_hash_of_port_res.end() ) {
                LOG_DEBUG << " port status was changed as NOT_USED in port source map";
                iter_ports.value() = NOT_USED;
            }
        }
        emit    signal_destory_me( m_confcall_number );
    }
    LOG_DEBUG << " m_hash_of_port_res : " << m_hash_of_port_res << ", size : " << m_hash_of_port_res.size();


    return true;
}

quint16
CVoIpServerConfCallWorker::get_available_port()
{
    QHash<quint16,bool>::iterator                  iter_ports;
    LOG_DEBUG << " m_hash_of_port_res : " << m_hash_of_port_res << ", size : " << m_hash_of_port_res.size();
    for( iter_ports = m_hash_of_port_res.begin() ; iter_ports != m_hash_of_port_res.end() ; iter_ports++ ) {
        if( iter_ports != m_hash_of_port_res.end() && NOT_USED == iter_ports.value() )  {
            LOG_DEBUG << " There is available port, num = " << iter_ports.key();
            return iter_ports.key();
        }
    }

    return 0;
}
