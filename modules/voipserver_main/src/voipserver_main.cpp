#include <QCoreApplication>
#include "voipserver_define.h"
#include "voipserver_config.h"
#include "voipserver_init.h"
#include <iostream>

CVoIpServerConfig g_config;

void myMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    (void)context;
    QString txt;
    switch (type) {
    case QtDebugMsg:
        txt = QString("%1").arg(msg);
        break;
    case QtWarningMsg:
        txt = QString("%1").arg(msg);
        break;
    case QtCriticalMsg:
        txt = QString("%1").arg(msg);
        break;
    case QtFatalMsg:
        txt = QString("%1").arg(msg);
        abort();
        break;
    case QtInfoMsg:
    default:
        break;
    }
    QFile outFile("/tmp/voipserver_main.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    std::cout << txt.toStdString() << std::endl;
    ts << txt << endl;
}

void print_help()
{
    std::cout << " Usage : voipserver [configfile]" << std::endl;
    std::cout << " [configfile] : default file is on /app/config/config_voipserver.json" << std::endl;
}

int main(int argc, char *argv[])
{
    qInstallMessageHandler(myMessageHandler);
    QCoreApplication a(argc, argv);

    QString configfile;
    if( argc != 2 ) {
        print_help();
        configfile = "/app/config/config_voipserver.json";
    }
    else {
        configfile = argv[1];
    }

    g_config.set_config(configfile);

    LOG_INFO << "********************************************************" ;
    LOG_INFO << "***          VoIpServer is Started               ***" ;
    LOG_INFO << "***          Process ID = " << a.applicationPid() << "                     ***" ;
    LOG_INFO << "********************************************************" ;

    if( MATCHED == g_config.m_function.toUpper().compare("SERVER") ) { // MATCHED SERVER FUNCTION
        LOG_INFO << " - function         : " << g_config.m_function.toUpper();
        LOG_INFO << " - server_addr      : " << g_config.m_server_addr;
        LOG_INFO << " - server_port      : " << g_config.m_server_port;
        LOG_INFO << " - keep_alive_port  : " << g_config.m_keepalive_port;

        new CVoIpServerInit();
    }
    else {
        LOG_ERROR << "Configuration file has problem, please check it..!!!";
        exit(0);
    }
    
    return a.exec();
}
