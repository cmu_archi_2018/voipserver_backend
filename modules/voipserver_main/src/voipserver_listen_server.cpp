#include "voipserver_listen_server.h"

CVoIpServerListenServer::CVoIpServerListenServer(quint16 serverport, QObject *parent) :
    //m_server_port(serverport),
    QTcpServer(parent)
{

    //m_server_port = 0;
    g_map_of_conn.clear();

    m_database_ = nullptr;
    m_confcall_ = nullptr;

    m_is_keepalive = false;

    m_server_port = serverport;


//    start_server(serverport);
}

CVoIpServerListenServer::~CVoIpServerListenServer()
{

}

void
CVoIpServerListenServer::set_keepalive()
{
    m_is_keepalive = true;
}

bool
CVoIpServerListenServer::set_database(CVoIpServerDataBase *database_)
{
    if( nullptr == database_ ) {
        LOG_ERROR << " database is NULL..!!! could NOT set..!!";
        return false;
    }

    m_database_ = database_;

    return true;
}

bool
CVoIpServerListenServer::set_confcall(CVoIpServerConfCall* confcall_)
{
    if( nullptr == confcall_ ) {
        LOG_ERROR << " confcall_ is NULL..!!! could not set pointer..!!";
        return false;
    }
    else {
        m_confcall_ = confcall_;
    }
    return true;
}

void
CVoIpServerListenServer::start_server( quint16 port )
{
    LOG_INFO << "TCP Server will start with port = " << port;

    if( 0 == port ) {
        LOG_ERROR << "Port number is wrong..!! Could not start server..!!, port = " << port;
        exit(0);
    }

    // double check port

    if( m_server_port != port ) {
        LOG_ERROR << " server port setup has wrong value..!!" ;
    }

    m_server_port = port;

    if( false == this->listen(QHostAddress::Any, m_server_port) ) {
        LOG_ERROR << "Could not start server..!!";
        exit(0);
    }
    else {
        LOG_INFO << "Listening to port = " << m_server_port << "...";
//        m_database_ = new CVoIpServerDataBase();
    }
}

void
CVoIpServerListenServer::incomingConnection( qintptr sock_fd )
{
    // a new connection
    // Every new connection will be run in a newly created thread
    CVoIpServerClientWorker *t = new CVoIpServerClientWorker(sock_fd);

    if( false == t->set_database(m_database_) ) {
        LOG_ERROR << "m_database class did NOT created..!!";
        return;
    }

    if( false == m_is_keepalive && false == t->set_confcall(m_confcall_) ) {
        LOG_ERROR << "m_confcall class did NOT created..!!";
        return;
    }


    QThread *th_ = new QThread(this);
    connect(th_, SIGNAL(finished()),                            t,         SLOT(deleteLater()));
    connect(th_, SIGNAL(started()),                             t,         SLOT(slot_do_work()));
    connect(t,   SIGNAL(signal_disconnecting(CVoIpServerClientWorker*)),  this,   SLOT(slot_disconnected(CVoIpServerClientWorker*)));

    t->moveToThread(th_);
    th_->start();

//    g_map_of_conn.insert(std::pair<CVoIpServerClientWorker*, CVoIpServerClientWorker*>(t, t));
    LOG_DEBUG << "Connection(" << th_ << ") be inserted..!!, remain conn = " << g_map_of_conn.size();
}

void
CVoIpServerListenServer::slot_disconnected(CVoIpServerClientWorker *thread)
{
    std::map<QString, CVoIpServerClientWorker*>::iterator iter;
    iter = g_map_of_conn.find(thread->m_caller_phone);

    if( iter != g_map_of_conn.end() ) {
        LOG_DEBUG << "Connection(" << thread << ") be removed..!!, remain conn = " << g_map_of_conn.size();
        g_map_of_conn.erase(iter);
    }
}
