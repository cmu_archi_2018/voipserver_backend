#include "voipserver_client_worker.h"

CVoIpServerClientWorker::CVoIpServerClientWorker(qintptr fd, QObject *parent) :
    QObject(parent)
{
    m_caller_sock_          = nullptr;
    m_caller_sock_fd        = 0;

    m_caller_addr.clear();
    m_caller_phone.clear();


    m_callee_sock_          = nullptr;
    m_callee_sock_fd        = 0;

    m_callee_addr.clear();
    m_callee_phone.clear();

    m_is_conf_call          = false;
    m_ringback_process      = nullptr;

    m_database_             = nullptr;
    m_confcall_             = nullptr;

    this->m_caller_sock_fd  = fd;

    m_ssl_local_certificate.clear();
    m_ssl_private_key.clear();
    m_ssl_protocol = QSsl::UnknownProtocol;

    if( QSsl::UnknownProtocol != g_config.m_ssl_protocol ) {
        if( false == set_ssl_local_certificate(g_config.m_ssl_certificate) ) {
            LOG_ERROR << " Can not set ssl_certificate...!!! ";
            return;
        }

        if( false == set_ssl_private_key(g_config.m_ssl_key) ) {
            LOG_ERROR << " Can not set ssl_private_key...!!! ";
            return;
        }
        set_ssl_protocol((QSsl::SslProtocol)g_config.m_ssl_protocol);
    }
}

CVoIpServerClientWorker::~CVoIpServerClientWorker()
{
}

QString
CVoIpServerClientWorker::parsing_addr(QString addr)
{
    if (addr.startsWith("::ffff:") == true )
        return addr.right(addr.length() - 7);
    return addr;
}

bool
CVoIpServerClientWorker::set_database(CVoIpServerDataBase* database_)
{
    if( nullptr == database_ ) {
        LOG_ERROR << " database_ is NULL..!!! could not set pointer..!!";
        return false;
    }
    else {
        m_database_ = database_;
    }
    return true;
}

bool
CVoIpServerClientWorker::set_confcall(CVoIpServerConfCall* confcall_)
{
    if( nullptr == confcall_ ) {
        LOG_ERROR << " confcall_ is NULL..!!! could not set pointer..!!";
        return false;
    }
    else {
        m_confcall_ = confcall_;
    }
    return true;
}

void
CVoIpServerClientWorker::slot_do_work()
{
    m_caller_sock_ = new QSslSocket(this);

    if( false == m_caller_sock_->setSocketDescriptor(this->m_caller_sock_fd) ) {
        delete m_caller_sock_;
        emit signal_error(m_caller_sock_->error());
        return;
    }

    if( QSsl::UnknownProtocol != g_config.m_ssl_protocol ) {
        LOG_INFO << " SSL Protocol : " << m_ssl_protocol;
        connect(m_caller_sock_, SIGNAL(encrypted()), this, SLOT(slot_ready_of_caller()));

        m_caller_sock_->setLocalCertificate(m_ssl_local_certificate);
        m_caller_sock_->setPrivateKey(m_ssl_private_key);
        m_caller_sock_->setProtocol(m_ssl_protocol);
        m_caller_sock_->startServerEncryption();
    }

    m_caller_addr = parsing_addr(m_caller_sock_->peerAddress().toString());
    LOG_DEBUG << "Sender(" << m_caller_addr << ") Thread started..!!";

    m_caller_sock_->moveToThread(this->thread());
    m_caller_sock_->setSocketOption(QAbstractSocket::SocketOption::LowDelayOption, 1);

    // connect socket and signal
    // note - Qt::DirectConnection is used because it's multithreaded
    //        This makes the slot to be invoked immediately, when the signal is emitted.
    connect(m_caller_sock_, SIGNAL(readyRead()),       this, SLOT(slot_read_of_caller()), Qt::DirectConnection);
    connect(m_caller_sock_, SIGNAL(disconnected()),    this, SLOT(slot_disconnect_of_caller()));
//    connect(m_caller_sock_, SIGNAL(connected()),       this, SLOT(slot_first_connection()));

    // We'll have multiple clients, we want to know which is which
    LOG_DEBUG << m_caller_sock_fd << " Client connected..!!, thread : " << this->thread();

    // waiting for connection completely...
    if( QSsl::UnknownProtocol != g_config.m_ssl_protocol ) {
        LOG_INFO << " Wait for encrypted..!!";
        m_caller_sock_->waitForEncrypted();
    }
    else {
        m_caller_sock_->waitForConnected();
    }
}

void
CVoIpServerClientWorker::slot_read_of_caller()
{
    mdb::DB_RESULT_E    ret_db;
    // get the information
    QByteArray data = m_caller_sock_->readAll();

    // will write on server side window
    LOG_DEBUG << "Receviced from Caller (IP:" << parsing_addr(m_caller_sock_->peerAddress().toString()) << ")";
//    LOG_DEBUG << "=> data " << data;

    QJsonDocument   doc     = QJsonDocument::fromJson(data.toStdString().c_str());
    QJsonObject     obj     = doc.object();
    QString         msg;

    if( true != g_config.m_token_secret_key.isEmpty() ) {
        LOG_DEBUG << " Validating Token is ENABLED..!!";
        if( 0 == obj.value("token").toString().size() || false == get_valid_token(obj.value("token").toString())) {
            LOG_ERROR << " This message's token is WRONG..!! DROP..!!";
            return;
        }
        else {
            LOG_DEBUG << " This token is VALID..!! continue processing message..";
        }
    }
    else {
        LOG_DEBUG << " Validating Token is DISABLED..!!, we will NOT check Token";
    }
    msg         = obj.value("msg").toString();


    if( MATCHED == msg.toUpper().compare("INVITE") ) {
        /* TBD
         * Check condition of RECV whether available recv INVITE or not, from Database
         */
        m_caller_phone = obj.value("from").toString();
        m_callee_phone = obj.value("to").toString();

        ret_db = m_database_->get_user_info_from_users( m_caller_phone, m_caller_email );
        if( NUMBER_VALID != ret_db ) {
            LOG_ERROR << " This caller is abnormal user, there is no Account...!!!";
            LOG_ERROR << "  without sending any message, Just Call Drop";
            disconnect();
            return;
        }

        m_call_request_date = QDateTime::currentDateTime();

        ret_db = m_database_->set_request_call(obj.value("cid").toString(), m_caller_phone, m_caller_email, m_call_request_date );
        if( DONE != ret_db ) {
            LOG_ERROR << " There is ERROR in database.. reason : " << m_database_->get_mdb_error_to_string(ret_db);
            disconnect();
            return;
        }

        LOG_DEBUG << " Caller : " << m_caller_phone << " == (INVITE1) ==> " << " Callee : " << m_callee_phone;

        ret_db = m_database_->get_user_address_from_directories(obj.value("to").toString(), m_callee_addr);
        if( NUMBER_NOT_VALID == ret_db ) {
            LOG_WARN << " Cound NOT find 'to' Callee Number in Database..!!";

            QJsonObject json_obj;
            json_obj["msg"] = "bye";
            json_obj["seqNo"] = obj.value("seqNo").toInt() + 1;
            json_obj["cid"] = obj.value("cid").toString();
            json_obj["reason"] = "4";

            QJsonDocument json_doc(json_obj);
            QString json_string = simplified_json_string(json_doc.toJson());

            LOG_DEBUG << " Server : " << g_config.m_server_addr << " == (BYE:4) ==> " << " Caller : " << m_caller_phone;

            m_caller_sock_->write(json_string.toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000); // for waiting sending message to destination

            return;
        }
        // Keepalive expired hosts.
        else if( NUMBER_NOT_ONLINE == ret_db ) {
            LOG_WARN << " Callee is NOT Online...!!";

            QJsonObject json_obj;
            json_obj["msg"] = "bye";
            json_obj["seqNo"] = obj.value("seqNo").toInt() + 1;
            json_obj["cid"] = obj.value("cid").toString();
            json_obj["reason"] = "5";

            QJsonDocument json_doc(json_obj);
            QString json_string = simplified_json_string(json_doc.toJson());

            LOG_DEBUG << " Server : " << g_config.m_server_addr << " == (BYE:5) ==> " << " Caller : " << m_caller_phone;
            m_caller_sock_->write(json_string.toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000); // for waiting sending message to destination
        }
//        else if( NUMBER_IN_CALLING == ret_db ) {
//            LOG_WARN << " Callee is in Calling...!!";

//            QJsonObject json_obj;
//            json_obj["msg"] = "bye";
//            json_obj["seqNo"] = obj.value("seqNo").toInt() + 1;
//            json_obj["cid"] = obj.value("cid").toString();
//            json_obj["reason"] = "100";

//            QJsonDocument json_doc(json_obj);
//            QString json_string = simplified_json_string(json_doc.toJson());

//            LOG_DEBUG << " Server : " << g_config.m_server_addr << " == (BYE:100) ==> " << " Caller : " << m_caller_phone;
//            m_caller_sock_->write(json_string.toUtf8());
//            m_caller_sock_->flush();

//            QThread::usleep(g_config.m_message_duration*1000); // for waiting sending message to destination
//        }
        // Conference Call Number
        else if( NUMBER_CONFERENCE == ret_db ) {
            // 1. send "TRYING" message,
            // 2. send "RINGING" message,
            // 3. get possible port to join
            // 4. send "ACCEPT" message, with possible port.
            m_is_conf_call = true;

            /*
             * Send "TRYING" to Sender.
             */
            QJsonObject json_obj;
            json_obj["msg"] = "trying";
            json_obj["seqNo"] = obj.value("seqNo").toInt();
            json_obj["cid"] = obj.value("cid").toString();

            QJsonDocument json_doc(json_obj);
            QString json_string = simplified_json_string(json_doc.toJson());
            LOG_DEBUG << " TRYING json : " << json_string;
            m_caller_sock_->write(json_string.toUtf8());
            m_caller_sock_->flush();
            /*
             * Send "TRYING" to Sender.
             */

            QThread::usleep(g_config.m_message_duration*1000);

            /*
             * Send "RINGING" to Sender.
             */
            json_string.clear();
            json_obj["msg"] = "ringing";
            json_obj["seqNo"] = obj.value("seqNo").toInt();
            json_obj["cid"] = obj.value("cid").toString();

            QJsonDocument ring_json_doc(json_obj);

            json_string = simplified_json_string(ring_json_doc.toJson());
            LOG_DEBUG << " RINGING json : " << json_string;
            m_caller_sock_->write(json_string.toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000*300);

            /*
             * Request to joining Confernece Call
             */
            quint16 ret_port = m_confcall_->ask_to_join(m_callee_phone, obj);

            if( 0 == ret_port ) {
                json_string.clear();
                json_obj["msg"] = "bye";
                json_obj["seqNo"] = obj.value("seqNo").toInt();
                json_obj["cid"] = obj.value("cid").toString();
                json_obj["reason"] = 6; // conference cannot made;

                QJsonDocument bye_json_doc(json_obj);

                json_string = simplified_json_string(bye_json_doc.toJson());
                LOG_WARN << " Conference Room could not made..!!";
                LOG_DEBUG << " BYE json : " << json_string;
                m_caller_sock_->write(json_string.toUtf8());
                m_caller_sock_->flush();

                return;
            }
            else {
                json_obj["msg"]         = "accept";
                json_obj["seqNo"]       = obj.value("seqNo").toInt();
                json_obj["cid"]         = obj.value("cid").toString();
                json_obj["rtp_ip"]      = g_config.m_server_addr;
                json_obj["rtp_port"]    = ret_port;

                QJsonDocument accept_json_doc(json_obj);

                json_string = simplified_json_string(accept_json_doc.toJson());
                LOG_WARN << " Conference Room was made..!!";
                LOG_DEBUG << " ACCEPT json : " << json_string;
                m_caller_sock_->write(json_string.toUtf8());
                m_caller_sock_->flush();
            }

            return;
        }
        else {
            LOG_INFO << " Found Recv IP Address : " << m_callee_addr;
            if( 0 == m_callee_addr.size() ) {
                LOG_ERROR << " m_callee_addr is malformed..!!";
                return;
            }
        }

        ret_db = m_database_->get_user_info_from_users( m_callee_phone, m_callee_email );
        if( NUMBER_VALID != ret_db ) {
            LOG_ERROR << " This callee is abnormal user, there is no Account...!!!";
            LOG_ERROR << "  without sending any message, Just Call Drop";
            disconnect();
            return;
        }

        /*
         * Make a socket to Recv
         * TBD
         * if SSL is available, We Should change this part.
         */
//        m_callee_sock_    = new QSslSocket();
//        m_callee_sock_->addCaCertificates(g_config.m_ssl_certificate);
//        m_callee_sock_->connectToHostEncrypted(g_config.recv_addr, g_config.m_client_signal_port );
        m_callee_sock_    = new QTcpSocket();
//        m_callee_sock_->connectToHost(g_config.m_test_callee_addr, g_config.m_client_signal_port );
        m_callee_sock_->connectToHost(m_callee_addr, g_config.m_client_signal_port );

        if( m_callee_sock_->waitForConnected(3000) ) {
            m_callee_sock_fd = m_callee_sock_->socketDescriptor();

            LOG_INFO << " SUCCESS : new callee_sock_fd = " << m_callee_sock_fd;
            /*
             * Connect sock to slot ( read from RECV )
             */
            connect(m_callee_sock_, SIGNAL(readyRead()),       this, SLOT(slot_read_of_callee()), Qt::DirectConnection);
            connect(m_callee_sock_, SIGNAL(disconnected()),    this, SLOT(slot_disconnect_of_callee()));

            /*
             * Send INVITE message to Recv
             * only delivery from Send
             */

            LOG_DEBUG << " Caller : " << m_caller_phone << " == (INVITE2) ==> " << " Callee : " << m_callee_phone;
            m_callee_sock_->write(simplified_json_string(data).toUtf8());
            m_callee_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000); // for waiting sending message to destination

            /*
             * Send TRYING to Send
             */
            {
                QJsonObject     obj_trying_json;
                obj_trying_json["msg"] = "trying";
                obj_trying_json["seqNo"] = obj.value("seqNo").toInt();
                obj_trying_json["cid"] = obj.value("cid").toString();

                QJsonDocument   doc_trying_json(obj_trying_json);
                QString  string_trying_json = doc_trying_json.toJson();

                LOG_DEBUG << " Server : " << g_config.m_server_addr << " == (TRYING) ==> " << " Caller : " << m_caller_phone;
                m_caller_sock_->write(simplified_json_string(string_trying_json).toUtf8());
                m_caller_sock_->flush();

                QThread::usleep(g_config.m_message_duration*1000); // for waiting sending message to destination
            }
        }
        else {
            LOG_WARN << " recv socket could NOT be made..!!";
            /*
            * Not Connected...!!! Call Drop!!!
            */
            QJsonObject json_obj;
            json_obj["msg"] = "bye";
            json_obj["seqNo"] = obj.value("seqNo").toInt() + 1;
            json_obj["cid"] = obj.value("cid").toString();
            json_obj["reason"] = "5";

            QJsonDocument json_doc(json_obj);
            QString json_string = simplified_json_string(json_doc.toJson());

            LOG_DEBUG << " Server : " << g_config.m_server_addr << " == (BYE:5) ==> " << " Caller : " << m_caller_phone;
            m_caller_sock_->write(json_string.toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000); // for waiting sending message to destination

            /* TBD
             * Send RINGBACK to Send;
             */
        }

    } // msg INVITE
    else if( MATCHED == msg.toUpper().compare("ACK") ) {
        LOG_DEBUG << " Caller : " << m_caller_phone << " == (ACK1) ==> " << " Callee : " << m_callee_phone;

        DB_RESULT_E ret_db = FAILED;
        m_call_start_date = QDateTime::currentDateTime();

        if( true == m_is_conf_call ) {
            if( false == m_confcall_->join_confcall(m_callee_phone, m_caller_addr, obj.value("rtp_port").toInt()) ) {
                QJsonObject json_obj;
                QString     json_string;

                json_obj["msg"] = "bye";
                json_obj["seqNo"] = obj.value("seqNo").toInt();
                json_obj["cid"] = obj.value("cid").toString();
                json_obj["reason"] = 6; // conference cannot made;

                QJsonDocument bye_json_doc(json_obj);

                json_string = simplified_json_string(bye_json_doc.toJson());
                LOG_WARN  << "Conference Room could NOT made..!!";
                LOG_DEBUG << " Server : " << g_config.m_server_addr << " == (BYE) ==> " << " Caller : " << m_caller_phone;
                m_caller_sock_->write(json_string.toUtf8());
                m_caller_sock_->flush();

                QThread::usleep(g_config.m_message_duration*1000); // for waiting sending message to destination
            }
            else {
                ret_db = m_database_->set_start_call(obj.value("cid").toString(), m_caller_phone, m_callee_phone, m_caller_email, m_callee_email, m_call_start_date );
                if( DONE != ret_db ) {
                    LOG_ERROR << " There is ERROR in database.. reason : " << m_database_->get_mdb_error_to_string(ret_db);
                    if( nullptr != m_caller_sock_ && QSslSocket::UnconnectedState != m_caller_sock_->state() ) emit m_caller_sock_->disconnect();
                    if( nullptr != m_callee_sock_ && QSslSocket::UnconnectedState != m_callee_sock_->state() ) emit m_callee_sock_->disconnect();
                    return;
                }
            }
            return;
        }

        /*
         * Send Message to Recv;
         */
        if( nullptr != m_callee_sock_ && QSslSocket::UnconnectedState != m_callee_sock_->state() ) {
            QString json_str = simplified_json_string(data);

            LOG_DEBUG << " Caller : " << m_caller_phone << " == (ACK2) ==> " << " Callee : " << m_callee_phone;
            m_callee_sock_->write(json_str.toUtf8());
            m_callee_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000); // for waiting sending message to destination

            LOG_DEBUG << " Caller email : " <<  m_caller_email << ", Callee email : " << m_callee_email;
            ret_db = m_database_->set_start_call(obj.value("cid").toString(), m_caller_phone, m_callee_phone, m_caller_email, m_callee_email, m_call_start_date );
            if( DONE != ret_db ) {
                LOG_ERROR << " There is ERROR in database.. reason : " << m_database_->get_mdb_error_to_string(ret_db);
                if( nullptr != m_caller_sock_ && QSslSocket::UnconnectedState != m_caller_sock_->state() ) emit m_caller_sock_->disconnect();
                if( nullptr != m_callee_sock_ && QSslSocket::UnconnectedState != m_callee_sock_->state() ) emit m_callee_sock_->disconnect();
                return;
            }
        }
    } // msg ACK
    else if( MATCHED == msg.toUpper().compare("BYE") ) {
        m_call_end_date = QDateTime::currentDateTime();
        LOG_INFO << " Call <BYE> Message, DateTime " << m_call_end_date.toString("yyyy-MM-dd, hh:mm:ss");
        DB_RESULT_E ret_db = FAILED;

        LOG_DEBUG << " Caller : " << m_caller_phone << " == (BYE) ==> " << " Callee : " << m_callee_phone;
        if( true == m_is_conf_call ) {
            if( false == m_confcall_->exit_confcall(m_callee_phone, m_caller_addr, g_config.m_client_media_port) ) {
                LOG_ERROR << " Could NOT exit from confcall : " << m_callee_phone;
                return;
            }

            ret_db = m_database_->set_end_call(obj.value("cid").toString(), m_call_start_date );
            if( DONE != ret_db ) {
                LOG_ERROR << " There is ERROR in database.. reason : " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }

            QList<DB_CACHED_TEXT>   t_list;
            m_database_->set_update_keepalive_standby( m_caller_phone, m_caller_addr, t_list );

            if( nullptr != m_caller_sock_ && QSslSocket::UnconnectedState != m_caller_sock_->state() )
                emit m_caller_sock_->disconnect();

            return;
        }

        /*
         * Send BYE to Recv;
         */

        if( nullptr != m_callee_sock_ && QSslSocket::UnconnectedState != m_callee_sock_->state() ) {
            QString json_str = simplified_json_string(data);

            m_callee_sock_->write(json_str.toUtf8());
            m_callee_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000); // for waiting sending message to destination

            QList<DB_CACHED_TEXT>   t_list;
            m_database_->set_update_keepalive_standby( m_caller_phone, m_caller_addr, t_list );
            m_database_->set_update_keepalive_standby( m_callee_phone, m_callee_addr, t_list );

            ret_db = m_database_->set_end_call(obj.value("cid").toString(), m_call_start_date );
            if( DONE != ret_db ) {
                LOG_ERROR << " There is ERROR in database.. reason : " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }
        }

        /* TBD
         * Clean Resource
         */
        if( nullptr != m_caller_sock_ && QSslSocket::UnconnectedState != m_caller_sock_->state() ) emit m_caller_sock_->disconnect();
        if( nullptr != m_callee_sock_ && QSslSocket::UnconnectedState != m_callee_sock_->state() ) emit m_callee_sock_->disconnect();

    } // msg BYE
    else if( MATCHED == msg.toUpper().compare("KEEPALIVE") ) {
        LOG_INFO << " KeppAlive Message ";
        // When server receive keepalive message from Client,
        // Server should uptate database of corresponding client
        // and check keepalive status.
        QString status = obj.value("status").toString();
        QString phoneno = obj.value("phoneNo").toString();

        bool    okay = true;

        if( MATCHED == status.toUpper().compare("STANDBY") ) {

            QList<DB_CACHED_TEXT>  cached_text_list;

            DB_RESULT_E     ret_db;

            ret_db = m_database_->set_update_keepalive_standby(phoneno, m_caller_addr, cached_text_list );

            if( (TEXT_CACHED == ret_db) && (cached_text_list.size() != 0) ) {
                /*
                 * Make a socket to Calle
                 * if SSL is available, We Should change this part.
                 */
                //m_callee_sock_    = new QSslSocket();
                //m_callee_sock_->addCaCertificates(g_config.m_ssl_certificate);
                //m_callee_sock_->connectToHostEncrypted(g_config.recv_addr, g_config.m_client_signal_port );
                m_callee_sock_    = new QTcpSocket();
                m_callee_sock_->connectToHost(m_caller_addr, g_config.m_client_signal_port );

                if( m_callee_sock_->waitForConnected(3000) ) {
                    m_callee_sock_fd = m_callee_sock_->socketDescriptor();

                    LOG_INFO << " SUCCESS : new callee_sock_fd = " << m_callee_sock_fd;
                    /*
                     * Connect sock to slot ( read from callee )
                     */
                    connect(m_callee_sock_, SIGNAL(readyRead()),       this, SLOT(slot_read_of_callee()), Qt::DirectConnection);
                    connect(m_callee_sock_, SIGNAL(disconnected()),    this, SLOT(slot_disconnect_of_callee()));

                    quint16     msg_seq_num = 1;

                    QJsonObject json_obj;
                    json_obj["msg"]     = "textmsg";

                    for( DB_CACHED_TEXT s : cached_text_list ) {
                        json_obj["seqNo"]   = msg_seq_num++;
                        json_obj["from"]    = s.caller;
                        json_obj["to"]      = s.callee;
                        json_obj["message"] = s.msg;

                        QJsonDocument json_doc(json_obj);
                        QString json_str = simplified_json_string(json_doc.toJson());

                        LOG_DEBUG << " Caller : " << s.caller << " == (TEXTMSG) ==> " << " Callee : " << s.callee << ", TEXTMSG : " << s.msg.left(10);
                        m_callee_sock_->write(json_str.toUtf8());
                        m_callee_sock_->flush();

                        QThread::usleep(g_config.m_message_duration * 1000);
                    }

                    // Update Databse of Messages
                    // Complete Sending messages
                    m_database_->set_complete_all_text_message(phoneno);
                }
                else if ( DONE != ret_db ) {
                    LOG_ERROR << " ERROR : " << m_database_->get_mdb_error_to_string(ret_db);
                    okay = false;
                }
            }
        }
        else if( MATCHED == status.toUpper().compare("CALLING") ) {
            ret_db = m_database_->set_update_keepalive_calling( m_caller_phone, m_caller_addr );
            if( DONE != ret_db ) {
                LOG_ERROR << " There is ERROR in database.. reason : " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }

            ret_db = m_database_->set_update_call(obj.value("cid").toString(), m_call_start_date );
            if( DONE != ret_db ) {
                LOG_ERROR << " There is ERROR in database.. reason : " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }
        }
        else {
            LOG_ERROR << " Malformed Request Message...!!!";
            okay = false;
        }

        if( nullptr != m_caller_sock_ && QSslSocket::UnconnectedState != m_caller_sock_->state() ) {
            QJsonObject     obj_ack_json;
            obj_ack_json["msg"] = "keepalive";
            obj_ack_json["status"] = (true == okay)?"ack":"nack";
            obj_ack_json["seqNo"] = obj.value("seqNo").toInt();

            QJsonDocument   doc_ack_json(obj_ack_json);
            QString  string_ack_json = doc_ack_json.toJson();

            m_caller_sock_->write(simplified_json_string(string_ack_json).toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration * 1000);

        }
    } // msg KEEPALIVE
    else if( MATCHED == msg.toUpper().compare("TEXTMSG") ) {
        /* TBD
         * Check condition of RECV whether available recv TEXTMSG or not, from Database
         * reason
         * 0: sended success
         * 1: internal error
         * 2: wrong number
         * 3: cached
         *
         * Response
         * {
         *  msg : textmsg,
         *  seqNo : 1
         *  status : ack | nack | cached,
         *  reason : 0
         *  from : 111-222-4444,
         *  to : 111-222-3333,
         *  token :
         * }
         */
        DB_RESULT_E ret_db = m_database_->get_user_address_from_directories(obj.value("to").toString(), m_callee_addr);

        QString     caller = obj.value("from").toString();
        QString     caller_email;
        QString     callee = obj.value("to").toString();
        QString     callee_email;
        QString     text_message = obj.value("message").toString();

        if( NUMBER_NOT_VALID == ret_db ) {
            LOG_WARN << " Could NOT find 'to' PhoneNumber in Database..!!";
            QJsonObject json_obj;

            json_obj["msg"]         = "textmsg";
            json_obj["seqNo"]       = obj.value("seqNo").toInt();
            json_obj["status"]      = "nack";
            json_obj["from"]        = obj.value("from").toString();
            json_obj["to"]          = obj.value("to").toString();
            json_obj["reason"]      = "2"; // wrong Number;

            QJsonDocument json_doc(json_obj);
            QString json_string = simplified_json_string(json_doc.toJson());

            LOG_DEBUG << " Server : " << g_config.m_server_addr << " == (TEXTMSG:nack) ==> " << " Caller : " << m_caller_phone;
            m_caller_sock_->write(json_string.toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000);

            return;
        }
        // Keepalive expired hosts.
        else if( NUMBER_NOT_ONLINE == ret_db ) {
            LOG_WARN << " Callee : " << obj.value("to").toString() << "is NOT online...!!";

            /* Get Caller Email Address */
            ret_db = m_database_->get_user_info_from_users(obj.value("from").toString(), caller_email);
            if( DONE != ret_db ) {
                LOG_ERROR << " ERROR : database code = " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }

            /* Get Callee Email Address */
            ret_db = m_database_->get_user_info_from_users(obj.value("to").toString(),   callee_email);
            if( DONE != ret_db ) {
                LOG_ERROR << " ERROR : database code = " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }

            /*
             * Text Message CACHED on Recv Phone Number
             */
            ret_db = m_database_->set_insert_text_message(caller,caller_email,callee,callee_email,text_message,false);
            if( DONE != ret_db ) {
                LOG_ERROR << " ERROR : database code = " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }

            /*
             * Send Response to Sender
             */
            QJsonObject json_obj;

            json_obj["msg"]         = "textmsg";
            json_obj["seqNo"]       = obj.value("seqNo").toInt();
            json_obj["status"]      = "cached";
            json_obj["from"]        = obj.value("from").toString();
            json_obj["to"]          = obj.value("to").toString();
            json_obj["reason"]      = "3"; // wrong Number;

            QJsonDocument json_doc(json_obj);
            QString json_string = simplified_json_string(json_doc.toJson());

            LOG_DEBUG << " Server : " << g_config.m_server_addr << " == (TEXTMSG:cached) ==> " << " Caller : " << m_caller_phone;
            m_caller_sock_->write(json_string.toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000);
            return;
        }
        else {
            LOG_INFO << " Found Callee IP Address : " << m_callee_addr;
        }

        /*
         * Make a socket to Callee
         * if SSL is available, We Should change this part.
         */
//        m_callee_sock_    = new QSslSocket();
//        m_callee_sock_->addCaCertificates(g_config.m_ssl_certificate);
//        m_callee_sock_->connectToHostEncrypted(g_config.recv_addr, g_config.m_client_signal_port );
        if( nullptr == m_callee_sock_ ) {
            m_callee_sock_    = new QTcpSocket();
            //        m_callee_sock_->connectToHost(g_config.m_test_callee_addr, g_config.m_client_signal_port );
            m_callee_sock_->connectToHost(m_callee_addr, g_config.m_client_signal_port );
        }

        if( m_callee_sock_->waitForConnected(3000) ) {
            /* Get Caller Email Address */
            ret_db = m_database_->get_user_info_from_users(obj.value("from").toString(), caller_email);
            if( NUMBER_VALID != ret_db ) {
                LOG_ERROR << " ERROR : database code = " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }

            /* Get Callee Email Address */
            ret_db = m_database_->get_user_info_from_users(obj.value("to").toString(),   callee_email);
            if( NUMBER_VALID != ret_db ) {
                LOG_ERROR << " ERROR : database code = " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }

            m_callee_sock_fd = m_callee_sock_->socketDescriptor();

            LOG_INFO << " new recv_sock_fd = " << m_callee_sock_fd;
            /* Connect sock to slot ( read from Callee ) */
            connect(m_callee_sock_, SIGNAL(readyRead()),       this, SLOT(slot_read_of_callee()), Qt::DirectConnection);
            connect(m_callee_sock_, SIGNAL(disconnected()),    this, SLOT(slot_disconnect_of_callee()));

            /*
             * Send TEXT Message to Callee
             * only delivery from Caller
             */
            LOG_DEBUG << " Caller : " << caller << " == (TEXTMSG) ==> " << " Callee : " << callee << ", TEXTMSG : " << text_message.left(10);
            m_callee_sock_->write(simplified_json_string(data).toUtf8());
            m_callee_sock_->flush();
            QThread::usleep(g_config.m_message_duration*1000);

            /*
             * Send TRYING to Send
             */
            {
                /*
             * Send Response to Sender
             */
                QJsonObject json_obj;
                json_obj["msg"]         = "textmsg";
                json_obj["seqNo"]       = obj.value("seqNo").toInt();
                json_obj["status"]      = "ack";
                json_obj["from"]        = obj.value("from").toString();
                json_obj["to"]          = obj.value("to").toString();
                json_obj["reason"]      = "0"; // DONEr;

                QJsonDocument json_doc(json_obj);
                QString json_string = simplified_json_string(json_doc.toJson());

                LOG_DEBUG << " Server : " << g_config.m_server_addr << " == (TEXTMSG:ack) ==> " << " Caller : " << caller;
                m_caller_sock_->write(json_string.toUtf8());
                m_caller_sock_->flush();
                QThread::usleep(g_config.m_message_duration*1000);
            }

            /*
             * Text Message CACHED on Recv Phone Number
             */
            ret_db = m_database_->set_insert_text_message(caller,caller_email,callee,callee_email,text_message,true);
            if( DONE != ret_db ) {
                LOG_ERROR << " ERROR : database code = " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }
        }
        else {
            LOG_WARN << " recv socket could NOT be made..!!";
            /*
             * Text Message Cached on Recv Phone Number
             */
            ret_db = m_database_->set_insert_text_message(caller,caller_email,callee,callee_email,text_message,false);
            if( DONE != ret_db ) {
                LOG_ERROR << " ERROR : database code = " << m_database_->get_mdb_error_to_string(ret_db);
                return;
            }

            /*
             * Send Response to Sender
             */
            QJsonObject json_obj;
            json_obj["msg"]         = "textmsg";
            json_obj["seqNo"]       = obj.value("seqNo").toInt();
            json_obj["status"]      = "cached";
            json_obj["from"]        = obj.value("from").toString();
            json_obj["to"]          = obj.value("to").toString();
            json_obj["reason"]      = "3"; // wrong Number;

            QJsonDocument json_doc(json_obj);
            QString json_string = simplified_json_string(json_doc.toJson());

            LOG_DEBUG << " Server : " << g_config.m_server_addr << " == (TEXTMSG:cached) ==> " << " Caller : " << m_caller_phone;

            m_caller_sock_->write(json_string.toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000);
        }
    } // msg.TEXTMSG

    /*
     * There might NOT be below command from Send
    else if( MATCHED == msg.toUpper().compare("ACCEPT") ) {
        LOG_INFO << " Call ACCEPT Message ";
    }
    else if( MATCHED == msg.toUpper().compare("REJECT") ) {
        LOG_INFO << " Call REJECT Message ";
    }
    */
}

void
CVoIpServerClientWorker::slot_read_of_callee()
{
    LOG_INFO << " slot_read_of_callee..!!";

    // get the information
    QByteArray data = m_callee_sock_->readAll();

    // will write on server side window
    LOG_DEBUG << "Receviced from recv " << parsing_addr(m_callee_sock_->peerAddress().toString());
//    LOG_DEBUG << "=> data " << data;

    QJsonDocument   doc     = QJsonDocument::fromJson(data.toStdString().c_str());
    QJsonObject     obj     = doc.object();
    QString         msg;

    if( true != g_config.m_token_secret_key.isEmpty() ) {
        LOG_DEBUG << " Validating Token is ENABLED..!!";
        //if( false == get_valid_token(obj.value("token").toString())) {
        if( 0 == obj.value("token").toString().size() || false == get_valid_token(obj.value("token").toString())) {
            LOG_ERROR << " This message's token is WRONG..!! DROP..!!";
            return;
        }
        else {
            LOG_DEBUG << " This token is valid..!! continue processing message..";
        }
    }
    else {
        LOG_DEBUG << " Validating Token is DISABLED..!!, we will NOT check Token";
    }

    msg             = obj.value("msg").toString();

    if( MATCHED == msg.toUpper().compare("BYE") ) {
        LOG_INFO << " Call BYE Message ";
        /*
         * Send BYE to Send;
         */
        LOG_DEBUG << " Callee : " << m_callee_phone << " == (BYE) ==> " << " Caller : " << m_caller_phone;
        if( nullptr != m_caller_sock_ && QSslSocket::UnconnectedState != m_caller_sock_->state() ) {
            QString json_str = simplified_json_string(data);
            m_caller_sock_->write(json_str.toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000);
        }

        /*
         * TBD
         * Write END TIME to DATABASE for calling between send and recv.
         */

        /* TBD
         * Clean Resource
         */
        if( nullptr != m_callee_sock_ && QSslSocket::UnconnectedState != m_callee_sock_->state() )
            emit m_callee_sock_->disconnect();
    }
    else if( MATCHED == msg.toUpper().compare("KEEPALIVE") ) {
        LOG_INFO << " KeepAlive Message ";
        // TBD
        // When server receive keepalive message from Client,
        // Server should uptate database of corresponding client
        // and check keepalive status.
        bool    okay = true;

        QString status = obj.value("status").toString();
        QString phoneno = obj.value("phoneNo").toString();

        if( MATCHED == status.toUpper().compare("CALLING") ) {
            // TBD
            // When database was made for billing system.
            // This part will be update...!!
        }
        else {
            LOG_ERROR << " Malformed Request Message...!!!";
            okay = false;
        }

        if( nullptr != m_callee_sock_ && QSslSocket::UnconnectedState != m_callee_sock_->state() ) {
            QJsonObject     obj_ack_json;
            obj_ack_json["msg"] = "keepalive";
            obj_ack_json["status"] = (true == okay)?"ack":"nack";
            obj_ack_json["seqNo"] = obj.value("seqNo").toInt();

            QJsonDocument   doc_ack_json(obj_ack_json);
            QString  string_ack_json = doc_ack_json.toJson();

            m_callee_sock_->write(simplified_json_string(string_ack_json).toUtf8());
            m_callee_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000);
        }
    }
    else if( MATCHED == msg.toUpper().compare("RINGING") ) {
        LOG_INFO << " Call RINGING Message ";
        /*
         * Send RINGING to Send
         */
        LOG_DEBUG << " Callee : " << m_callee_phone << " == (RINGING) ==> " << " Caller : " << m_caller_phone;
        if( nullptr != m_caller_sock_ && QSslSocket::UnconnectedState != m_caller_sock_->state() ) {
            QJsonObject     obj_ringing_json;

            obj_ringing_json = obj;
            obj_ringing_json.value("seqNo") = obj.value("seqNo").toInt() + 1;

            QJsonDocument   doc_ringing_json(obj_ringing_json);
            QString  string_ringing_json = doc_ringing_json.toJson();

            m_caller_sock_->write(simplified_json_string(string_ringing_json).toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000);
        }

        /*
         * Ringback Tone would be send to Send
         */
        /*
        QString        ringback = g_config.m_ringback_bin;
        QStringList    arguments;

        m_ringback_process = new QProcess(this);
        arguments << g_config.m_config_file << m_caller_addr << "ring" ;

        m_ringback_process->start(ringback, arguments);
        connect( this, SIGNAL(signal_stop_ringback()), m_ringback_process, SLOT(terminate()), Qt::DirectConnection );
        */
    }
    else if( MATCHED == msg.toUpper().compare("ACCEPT") ) {
        //LOG_INFO << " Call ACCEPT Message ";
        /*
         * Send ACCEPT to Send
         */
        LOG_DEBUG << " Callee : " << m_callee_phone << " == (ACCEPT) ==> " << " Caller : " << m_caller_phone;
        if( nullptr != m_caller_sock_ && QSslSocket::UnconnectedState != m_caller_sock_->state() ) {
            QJsonObject     obj_accept_json;

            obj_accept_json = obj;

            obj_accept_json.value("seqNo") = obj.value("seqNo").toInt() + 1;

            QJsonDocument   doc_accept_json(obj_accept_json);
            QString  string_accept_json = doc_accept_json.toJson();

            m_caller_sock_->write(simplified_json_string(string_accept_json).toUtf8());
            m_caller_sock_->flush();

            QThread::usleep(g_config.m_message_duration*1000);

        }

        /*
        if( nullptr != m_ringback_process && QProcess::NotRunning != m_ringback_process->state()  ) {
            emit  signal_stop_ringback();
        }
        */

        /*
         * TBD
         * Write START TIME to DATABASE for calling between send and recv.
         */
    }
    else if( MATCHED == msg.toUpper().compare("REJECT") ) {
        LOG_INFO << " Call REJECT Message ";
        /* TBD
         * Send REJECT to Send
         */
        LOG_DEBUG << " Callee : " << m_callee_phone << " == (REJECT) ==> " << " Caller : " << m_caller_phone;
        if( nullptr != m_caller_sock_ && QSslSocket::UnconnectedState != m_caller_sock_->state() ) {
            QJsonObject     obj_reject_json;

            obj_reject_json = obj;

            obj_reject_json.value("seqNo") = obj.value("seqNo").toInt() + 1;

            QJsonDocument   doc_reject_json(obj_reject_json);
            QString         string_reject_json = doc_reject_json.toJson();

            m_caller_sock_->write(simplified_json_string(string_reject_json).toUtf8());
            m_caller_sock_->flush();

            if( false == m_caller_sock_->waitForBytesWritten(g_config.m_message_duration*10) ) {
                LOG_ERROR << " data written was error..., Retry!!";
                m_caller_sock_->write(simplified_json_string(string_reject_json).toUtf8());
                m_caller_sock_->flush();
            }

            QThread::usleep(g_config.m_message_duration*1000);
        }

        /*
         * Clean Resource
         */
        /*
        if( nullptr != m_ringback_process && QProcess::NotRunning != m_ringback_process->state()  ) {
            emit  signal_stop_ringback();
        }
        */
    }
}

QString
CVoIpServerClientWorker::simplified_json_string(QString json_string)
{
    json_string = json_string.simplified();
    json_string = json_string.remove(QRegExp("[\r\n]"));
    json_string.append("\r\n");
    return json_string;
}

void
CVoIpServerClientWorker::slot_disconnect_of_caller()
{
    LOG_INFO << " m_caller_addr : " << m_caller_addr << "(" << m_caller_sock_fd << "), Disconnected..!!";

    if( nullptr != m_ringback_process )  {
        m_ringback_process->terminate();
        m_ringback_process = nullptr;
    }

    if( m_is_conf_call ) {
        m_confcall_->exit_confcall(m_callee_phone,m_caller_addr,g_config.m_client_media_port);
    }

    emit signal_disconnecting(this);

    m_caller_sock_->deleteLater();
    m_caller_sock_    = nullptr;
    m_caller_sock_fd  = -1;
}

void
CVoIpServerClientWorker::slot_ready_of_caller()
{
    LOG_INFO << " slot_ready was called by sslsocket..!!";
}

void
CVoIpServerClientWorker::slot_disconnect_of_callee()
{
    LOG_INFO << " m_callee_addr : " << m_callee_addr << "(" << m_callee_sock_fd << "), Disconnected..!!";

//    emit signal_disconnecting(this);

    m_callee_sock_->deleteLater();
    m_callee_sock_    = nullptr;
    m_callee_sock_fd  = -1;
}

void
CVoIpServerClientWorker::slot_ready_of_callee()
{
    LOG_INFO << " slot_ready was called by sslsocket..!!";
}

QSslCertificate&
CVoIpServerClientWorker::get_ssl_local_certificate()
{
    return m_ssl_local_certificate;
}

QSslKey&
CVoIpServerClientWorker::get_ssl_private_key()
{
    return m_ssl_private_key;
}

QSsl::SslProtocol
CVoIpServerClientWorker::get_ssl_protocol()
{
    return m_ssl_protocol;
}

void
CVoIpServerClientWorker::set_ssl_local_certificate(const QSslCertificate &certificate)
{
    m_ssl_local_certificate = certificate;
}

bool
CVoIpServerClientWorker::set_ssl_local_certificate(const QString &path, QSsl::EncodingFormat format)
{
    QFile certificateFile(path);

    if( 0 == certificateFile.open(QIODevice::ReadOnly) )
        return false;

    m_ssl_local_certificate = QSslCertificate( certificateFile.readAll(), format);

    return true;
}

void
CVoIpServerClientWorker::set_ssl_private_key(const QSslKey &key)
{
    m_ssl_private_key = key;
}

bool
CVoIpServerClientWorker::set_ssl_private_key(const QString &fileName, QSsl::KeyAlgorithm algorithm, QSsl::EncodingFormat format, const QByteArray &passPhrase)
{
    QFile keyFile(fileName);

    if( 0 ==  keyFile.open(QIODevice::ReadOnly) )
        return false;

    m_ssl_private_key = QSslKey(keyFile.readAll(), algorithm, format, QSsl::PrivateKey, passPhrase);
    return true;
}

void
CVoIpServerClientWorker::set_ssl_protocol(QSsl::SslProtocol protocol)
{
    m_ssl_protocol = protocol;
}

bool
CVoIpServerClientWorker::get_valid_token(QString qtoken)
{
    if( 0 == qtoken.size() ) {
        LOG_ERROR << " qtoken size is 0..!! DROP...!!!";
        return false;
    }

    using namespace jwt::params;
    //Decode
    std::string token = qtoken.toStdString();
    std::error_code     ec;

    auto dec_obj = jwt::decode(token, \
                               algorithms({"HS256"}), \
                               ec, \
                               secret(g_config.m_token_secret_key.toStdString()), \
                               issuer(g_config.m_token_issuer.toStdString()));

    if( 0 != ec.value() ) {
        LOG_ERROR << " This token is WRONG..!! DROP return false...!!";
        LOG_ERROR << "  => reason : " << ec.message().c_str();
        return false;
    }

    return true;
}
