#include "voipserver_database.h"

QMutex      g_mutex;

CVoIpServerDataBase::CVoIpServerDataBase(QObject *parent) : QObject(parent)
{
    //    m_network_manager = nullptr;
    //    m_network_manager = new QNetworkAccessManager(this);
    //    connect( m_network_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slot_reply_finished(QNetworkReply*)));

    mongocxx::instance  inst{};

    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    try {
        auto collection     = client["studio-project"]["chargepolicies"];

        QByteArray      json_data;
        QJsonDocument   json_doc;
        QJsonObject     json_obj;

        bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result =
                collection.find_one(document{} << "isCurrent" << "Y" << finalize);

        // if fount value
        if(maybe_result) {
            json_data   = QByteArray::fromStdString(bsoncxx::to_json(*maybe_result));
            json_doc    = QJsonDocument::fromJson(json_data.toStdString().c_str());
            json_obj    = json_doc.object();

            m_charge_of_minute  = json_obj.value("chargeofMinute").toDouble();
            m_charge_of_message = json_obj.value("chargeofMessage").toDouble();
            LOG_DEBUG << " charge_of_minute    : " << m_charge_of_minute <<
                         ", charge_of_message  : " << m_charge_of_message;
        }

    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        return;
//        std::cout << e.what() << std::endl;
    }
}

DB_RESULT_E
CVoIpServerDataBase::get_user_address_from_directories(QString phone_number, QString& ret_addr )
{
//    return NUMBER_CONFERENCE;
    DB_RESULT_E    ret = NUMBER_NOT_VALID;

    ret_addr = "";

//    return ret_val;
    LOG_DEBUG << " -> phone_number : " << phone_number;
    try {
        mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
        mongocxx::client    client{uri};

        //    bsoncxx::builder::stream::document document{};

        {
            auto collection     = client["studio-project"]["directories"];
            //        auto cursor = collection.find({});

            QByteArray      json_data;
            QJsonDocument   json_doc;
            QJsonObject     json_obj;

            bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result =
                    collection.find_one(document{} << "phoneNo" << phone_number.toStdString() << finalize);

            // if fount value
            if(maybe_result) {
                json_data   = QByteArray::fromStdString(bsoncxx::to_json(*maybe_result));
                json_doc    = QJsonDocument::fromJson(json_data.toStdString().c_str());
                json_obj    = json_doc.object();

                ret_addr    = json_obj.value("ipAddress").toString();

//                if( MATCHED == json_obj.value("status").toString().toUpper().compare("CALLING") ) {
//                    ret         = NUMBER_IN_CALLING;
//                }
//                else if( MATCHED == json_obj.value("status").toString().toUpper().compare("STANDBY") ) {
//                    ret         = NUMBER_VALID;
//                }
                //if( MATCHED == json_obj.value("status").toString().toUpper().compare("STANDBY") ) {
                ret         = NUMBER_VALID;
                LOG_DEBUG << " phone_number    : " << phone_number<< ", db Phone Number : " << json_obj.value("phoneNo").toString();
            }

            /*
             { "_id" : { "$oid" : "5b30055fe0fb4f283869fb21" },
             "updateDate" : { "$date" : 1529874915654 },
            */
            QJsonObject t_obj = json_obj.value("updateDate").toObject();
//            LOG_DEBUG << " t_obj : " << t_obj;
//            LOG_DEBUG << "t_obj size : " << t_obj.size() << ", t_obj.value($date).toDouble : " << t_obj.value("$date").toDouble();
            quint64  last_updated_time = t_obj.value("$date").toDouble();

            /*
             * check if
             * this user has expired keepalive message
             */
            quint64  current_time = bsoncxx::types::b_date(std::chrono::system_clock::now());
            quint64  time_gap = current_time - last_updated_time;

            std::chrono::minutes    interval(g_config.m_keepalive_interval);

            LOG_DEBUG << " last_updated_time(ms from Epoch) : " << last_updated_time << \
                         ", current_time(ms from Epoch) : " << current_time << \
                         ", gap(ms) : " << time_gap << ", interval(Min) : " << g_config.m_keepalive_interval;

            if( (qint64)time_gap > std::chrono::microseconds(interval).count() * g_config.m_keepalive_missing ) {
                ret_addr = "X.X.X.X";
                ret      = NUMBER_NOT_ONLINE;
            }
        }

        // if cannot find from user directory
        // To check in Conference Room Database
        if( NUMBER_NOT_VALID == ret || NUMBER_NOT_ONLINE == ret ) {
            auto collection     = client["studio-project"]["conferences"];

            QByteArray      json_data;
            QJsonDocument   json_doc;
            QJsonObject     json_obj;

            bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result =
                    collection.find_one(document{} << "conferencePhoneNo" << phone_number.toStdString() << finalize);

            // if fount value
            if(maybe_result) {
                json_data   = QByteArray::fromStdString(bsoncxx::to_json(*maybe_result));
                json_doc    = QJsonDocument::fromJson(json_data.toStdString().c_str());
                json_obj    = json_doc.object();

                ret_addr     = json_obj.value("ipAddress").toString();
                LOG_DEBUG << " phone_number    : " << phone_number<< ", db Phone Number : " << json_obj.value("phoneNo").toString();

                quint64 start_time  = json_obj.value("startDate").toInt();
                quint64 end_time    = json_obj.value("endDate").toInt();
                quint64 current_time = bsoncxx::types::b_date(std::chrono::system_clock::now());


                LOG_DEBUG << " CONFCALL : startDate = " << start_time << ", endDate = " << end_time;

                // This Conference Call is not opend or has closed.
                if( current_time < start_time && current_time >= end_time ) {
                    ret_addr     = "";
                    ret          = NUMBER_NOT_VALID;
                }
                else {
                    LOG_DEBUG << " This call number is Conference Call Number";
                    ret_addr     = "C.C.C.C";
                    ret          = NUMBER_CONFERENCE;
                }
            } // if(maybe_result)
        } // if( MATCHED == ret_val.compare("0.0.0.0")  ) {

        LOG_DEBUG << " ret = " << get_mdb_error_to_string(ret);
       return ret;
    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        return FAILED;
    }
}

DB_RESULT_E
CVoIpServerDataBase::get_user_info_from_users(QString user_number, QString& user_email)
{
//    return NUMBER_VALID;
    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    auto collection     = client["studio-project"]["users"];

    QByteArray      json_data;
    QJsonDocument   json_doc;
    QJsonObject     json_obj;

    try {
        bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result =
                collection.find_one(document{} << "phoneNo" << user_number.toStdString() << finalize);

        // if fount value
        if(maybe_result) {
            json_data   = QByteArray::fromStdString(bsoncxx::to_json(*maybe_result));
            json_doc    = QJsonDocument::fromJson(json_data.toStdString().c_str());
            json_obj    = json_doc.object();

            user_email  = json_obj.value("email").toString();

            LOG_DEBUG << " userNumber : " << user_number << ", email : " << user_email;
            return NUMBER_VALID;
        }

        return NUMBER_NOT_VALID;

    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        return FAILED;
    }
}

DB_RESULT_E
CVoIpServerDataBase::set_update_keepalive_calling(QString phonenumber, QString ipaddr)
{
    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    try {
        auto collection     = client["studio-project"]["directories"];

        QMutexLocker    locker(&g_mutex);
        bsoncxx::stdx::optional<mongocxx::result::update> result =
                collection.update_one(document{} << "phoneNo" << phonenumber.toStdString() << finalize,
                                      document{} << "$set" << open_document <<
                                      "phoneNo"   << phonenumber.toStdString() <<
                                      "ipAddress" << ipaddr.toStdString() <<
                                      "updateDate" << bsoncxx::types::b_date(std::chrono::system_clock::now()) <<
                                      "status" << "calling" <<
                                      close_document << finalize);
        if( !result ) {
            LOG_ERROR << " could NOT modified phoneNo : " << phonenumber << "...!!";
            return NO_DATA_IN_DATABASE;
        }

        LOG_DEBUG << " Modified count : " << result->modified_count();
        return DONE;
    }
    catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        g_mutex.unlock();
        return FAILED;
    }
}

DB_RESULT_E
CVoIpServerDataBase::set_update_keepalive_standby(QString phonenumber, QString ipaddr, QList<DB_CACHED_TEXT>& cached_text)
{
    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    try {
        {
            auto collection     = client["studio-project"]["directories"];

            QMutexLocker    locker(&g_mutex);
            bsoncxx::stdx::optional<mongocxx::result::update> result =
                    collection.update_one(document{} << "phoneNo" << phonenumber.toStdString() << finalize,
                                          document{} << "$set" << open_document <<
                                          "phoneNo"   << phonenumber.toStdString() <<
                                          "ipAddress" << ipaddr.toStdString() <<
                                          "updateDate" << bsoncxx::types::b_date(std::chrono::system_clock::now()) <<
                                          "status" << "standby" <<
                                          close_document << finalize);
            if( !result ) {
                LOG_ERROR << " could NOT modified phoneNo : " << phonenumber << "...!!";
                return NO_DATA_IN_DATABASE;
            }

            LOG_DEBUG << " Modified count : " << result->modified_count();
        }

        {
            auto collection     = client["studio-project"]["messages"];

            mongocxx::cursor cursor = collection.find(
                        document{} << "calleePhoneNo" << phonenumber.toStdString() <<
                        "isComplete" << false << finalize);

            QByteArray      json_data;
            QJsonDocument   json_doc;
            QJsonObject     json_obj;
            DB_CACHED_TEXT  t_cached;

            for(auto doc : cursor) {
                std::cout << bsoncxx::to_json(doc) << "\n";


                json_data   = QByteArray::fromStdString(bsoncxx::to_json(doc));
                json_doc    = QJsonDocument::fromJson(json_data.toStdString().c_str());
                json_obj    = json_doc.object();

                t_cached.msg    = json_obj.value("textMessage").toString();
                t_cached.callee = json_obj.value("calleePhoneNo").toString();
                t_cached.caller = json_obj.value("callerPhoneNo").toString();

                cached_text.append(t_cached);
            }

            if( 0 != cached_text.size() ) {
                return TEXT_CACHED;
            }
        }

        return DONE;
    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        g_mutex.unlock();
        return FAILED;
    }
}

DB_RESULT_E
CVoIpServerDataBase::set_complete_all_text_message(QString callee)
{
    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    try {
        {
            auto collection     = client["studio-project"]["messages"];

            QMutexLocker    locker(&g_mutex);
            bsoncxx::stdx::optional<mongocxx::result::update> result =
                    collection.update_many(document{} << "calleePhoneNo" << callee.toStdString() <<
                                           "isComplete" << false << finalize,
                                           document{} << "$set" << open_document <<
                                           "calleePhoneNo"  << callee.toStdString() <<
                                           "sendDate" << bsoncxx::types::b_date(std::chrono::system_clock::now())<<
                                           "payYearMonth"  << QDateTime::currentDateTime().toString("yyyyMM").toStdString() <<
                                           "isComplete" << true << close_document << finalize);
            if( !result ) {
                LOG_ERROR << " could NOT modified phoneNo : " << callee << "...!!";
                return NO_DATA_IN_DATABASE;
            }

            LOG_DEBUG << " Modified count : " << result->modified_count();
        }

        return DONE;
    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        g_mutex.unlock();
        return FAILED;
    }
}

DB_RESULT_E
CVoIpServerDataBase::set_insert_text_message(QString caller, QString caller_email, QString callee, QString callee_email, QString msg, bool is_complete )
{
    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    try {
        {
            auto collection     = client["studio-project"]["messages"];
            bsoncxx::stdx::optional<mongocxx::result::insert_one> result;

            if( true == is_complete ) {
                QMutexLocker    locker(&g_mutex);
                result = collection.insert_one( document{} <<
                                                "callerPhoneNo" << caller.toStdString() <<
                                                "callerEmail" << caller_email.toStdString() <<
                                                "calleePhoneNo" << callee.toStdString() <<
                                                "calleeEmail" << callee_email.toStdString() <<
                                                "isComplete"  << true <<
                                                "sendDate" << bsoncxx::types::b_date(std::chrono::system_clock::now()) <<
                                                "requestDate" << bsoncxx::types::b_date(std::chrono::system_clock::now()) <<
                                                "textMessage" << msg.toStdString() <<
                                                "textLength" << msg.size() <<
                                                "chargePerMessage" << m_charge_of_message <<
                                                "payYearMonth"  << QDateTime::currentDateTime().toString("yyyyMM").toStdString() <<
                                                finalize );

            }
            else {
                QMutexLocker    locker(&g_mutex);
                result = collection.insert_one( document{} <<
                                                "callerPhoneNo" << caller.toStdString() <<
                                                "callerEmail" << caller_email.toStdString() <<
                                                "calleePhoneNo" << callee.toStdString() <<
                                                "calleeEmail" << callee_email.toStdString() <<
                                                "isComplete"  << false <<
                                                "requestDate" << bsoncxx::types::b_date(std::chrono::system_clock::now()) <<
                                                "textMessage" << msg.toStdString() <<
                                                "textLength" << msg.size() <<
                                                "chargePerMessage" << m_charge_of_message <<
                                                "payYearMonth"  << QDateTime::currentDateTime().toString("yyyyMM").toStdString() <<
                                                finalize );

            }

            if( !result ) {
                LOG_ERROR << " could NOT insert text message, caller : " << caller << "=>> calleee " << callee << ", msg : " << msg.left(10) << "...";
                return FAILED;
            }

//            LOG_DEBUG << " Inserted ID : " << result->inserted_id().get_int32();
        }
        return DONE;
    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        g_mutex.unlock();
        return FAILED;
    }
}

DB_RESULT_E
CVoIpServerDataBase::set_request_call(QString cid,  QString caller, QString caller_email, QDateTime request_date )
{
//    return DONE;
    (void)request_date;
    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    try {
        LOG_DEBUG << " request db, caller : " << caller << ", cid : " << cid ;
        auto collection     = client["studio-project"]["callhistories"];
        bsoncxx::stdx::optional<mongocxx::result::insert_one> result;
        QMutexLocker    locker(&g_mutex);
        result = collection.insert_one( document{} <<
                                        "cid"           << cid.toStdString() <<
                                        "callerPhoneNo" << caller.toStdString() <<
                                        "callerEmail"   << caller_email.toStdString() <<
                                        //"requestDate"   << bsoncxx::types::b_date(request_date.toMSecsSinceEpoch()) <<
                                        "requestDate"     << bsoncxx::types::b_date(std::chrono::system_clock::now()) <<
                                        "chargePerMinute" << m_charge_of_minute << finalize );

        if( !result ) {
            LOG_ERROR << " could NOT insert request call, caller : " << caller;
            return FAILED;
        }

//        LOG_DEBUG << " Inserted ID : " << result->inserted_id().get_int32();
        return DONE;
    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        g_mutex.unlock();
        return FAILED;
    }
}

/*duration, calleeEmail, chargePerMinute, payYearMonth, chargeOfTotal;*/

DB_RESULT_E
CVoIpServerDataBase::set_start_call(QString cid, QString caller, QString callee, QString caller_email, QString callee_email, QDateTime start_date )
{
//    return DONE;
    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    try {
        LOG_DEBUG << " startcall db, caller : " << caller << ", callee : " << callee << ", cid : " << cid ;
        auto collection     = client["studio-project"]["callhistories"];
        bsoncxx::stdx::optional<mongocxx::result::update> result;
        QMutexLocker    locker(&g_mutex);
        result = collection.update_one( document{} <<
                                        "cid"           << cid.toStdString() << finalize,
                                        document{} << "$set" << open_document <<
                                        "callerPhoneNo" << caller.toStdString() <<
                                        "callerEmail"   << caller_email.toStdString() <<
                                        "calleePhoneNo" << callee.toStdString() <<
                                        "calleeEmail"   << callee_email.toStdString() <<
                                        "startDate"     << bsoncxx::types::b_date(std::chrono::system_clock::now()) <<
                                        //"startDate"     << bsoncxx::types::b_date(start_date.toMSecsSinceEpoch()) <<
                                        "payYearMonth"  << start_date.toString("yyyyMM").toStdString() <<
                                        close_document << finalize );

        if( !result ) {
            LOG_ERROR << " could NOT update start call info, cid : " << cid;
            return NO_DATA_IN_DATABASE;
        }

        LOG_DEBUG << " Updated cnt : " << result->modified_count();
        return DONE;
    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        g_mutex.unlock();
        return FAILED;
    }
}

DB_RESULT_E
CVoIpServerDataBase::set_update_call(QString cid, QDateTime start_date)
{
    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    QDateTime           current_date = QDateTime::currentDateTime();
    quint64             duration_ms  = current_date.toMSecsSinceEpoch() - start_date.toMSecsSinceEpoch();
    QTime               duration_qtime = QTime::fromMSecsSinceStartOfDay(duration_ms);

    try {
        LOG_DEBUG << " update db, cid : " << cid ;
        auto collection     = client["studio-project"]["callhistories"];
        bsoncxx::stdx::optional<mongocxx::result::update> result;
        QMutexLocker    locker(&g_mutex);
        result = collection.update_one( document{} <<
                                        "cid"           << cid.toStdString() << finalize,
                                        document{} << "$set" << open_document <<
                                        //"endDate"     << bsoncxx::types::b_date(std::chrono::system_clock::now()) <<
                                        //"endDate"     << bsoncxx::types::b_date(start_date.toMSecsSinceEpoch()) <<
                                        "duration"    << duration_qtime.minute() <<
                                        close_document << finalize );

        if( !result ) {
            LOG_ERROR << " could NOT update call info, cid : " << cid;
            return NO_DATA_IN_DATABASE;
        }

        LOG_DEBUG << " Updated cnt : " << result->modified_count();
        return DONE;
    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        g_mutex.unlock();
        return FAILED;
    }
}

DB_RESULT_E
CVoIpServerDataBase::set_end_call(QString cid, QDateTime start_date )
{
//    return DONE;
    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    QDateTime           current_date = QDateTime::currentDateTime();
    quint64             duration_ms  = current_date.toMSecsSinceEpoch() - start_date.toMSecsSinceEpoch();
    QTime               duration_qtime = QTime::fromMSecsSinceStartOfDay(duration_ms);

    try {
        LOG_DEBUG << " update end call db, cid : " << cid ;
        auto collection     = client["studio-project"]["callhistories"];
        bsoncxx::stdx::optional<mongocxx::result::update> result;
        QMutexLocker    locker(&g_mutex);
        result = collection.update_one( document{} <<
                                        "cid"           << cid.toStdString() << finalize,
                                        document{} << "$set" << open_document <<
                                        "endDate"     << bsoncxx::types::b_date(std::chrono::system_clock::now()) <<
                                        "duration"    << duration_qtime.minute() <<
//                                        "endDate"     << bsoncxx::types::b_date(current_date.toMSecsSinceEpoch()) <<
                                        close_document << finalize );

        if( !result ) {
            LOG_ERROR << " could NOT update end call info, cid : " << cid;
            return NO_DATA_IN_DATABASE;
        }

        LOG_DEBUG << " Updated cnt : " << result->modified_count();
        return DONE;
    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        g_mutex.unlock();
        return FAILED;
    }
}


DB_RESULT_E
CVoIpServerDataBase::get_valid_user_of_conf_call(QString user_number, QString confcall_number)
{
    mongocxx::uri       uri(g_config.m_database_uri.toStdString().c_str());
    mongocxx::client    client{uri};

    LOG_DEBUG << " find user_number : " << user_number << " in confcall_number " << confcall_number;

    try {
        {
            auto collection     = client["studio-project"]["conferences"];

            bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result =
                    collection.find_one(document{} << "conferencePhoneNo" << confcall_number.toStdString() << finalize);

            // if fount value
            if(maybe_result) {
                QByteArray      json_data;
                QJsonDocument   json_doc;
                QJsonObject     json_obj;

                json_data   = QByteArray::fromStdString(bsoncxx::to_json(*maybe_result));
                json_doc    = QJsonDocument::fromJson(json_data.toStdString().c_str());
                json_obj    = json_doc.object();

                QJsonObject     json_obj_date;
                QDateTime       date_current;   date_current.currentDateTime();

                quint64         date_ms_start;
                quint64         date_ms_end;
                quint64         date_ms_current = date_current.toMSecsSinceEpoch();

                json_obj_date   = json_obj.value("startDate").toObject();
                date_ms_start   = json_obj_date.value("$date").toDouble();

                json_obj_date   = json_obj.value("endDate").toObject();
                date_ms_end     = json_obj_date.value("$date").toDouble();


                QDateTime   date_start;     date_start.fromMSecsSinceEpoch(date_ms_start);
                QDateTime   date_end;       date_end.fromMSecsSinceEpoch(date_ms_end);

                LOG_DEBUG << " This conference call has information as follow";
                LOG_DEBUG << "      - date_start   : " << date_start.toString();
                LOG_DEBUG << "      - date_end     : " << date_end.toString();
                LOG_DEBUG << "      - date_current : " << date_current.toString();

                if( date_ms_start > date_ms_current && date_ms_end < date_ms_current ) {
                    LOG_DEBUG << " This conferencecall is NOT valid NOW....!!";
                    return NOT_VALID_CONFCALL;

                }

                /*
                 * In case of HOST of conference call
                 */
                if( MATCHED == json_obj.value("hostPhoneNo").toString().compare(user_number) )  {
                    LOG_DEBUG << " user_number : " << user_number << ", is correct HOST in Conference Room";
                    return USER_VALID_OF_CONFCALL;
                }

                QJsonArray     json_array = json_obj.value("guestPhoneNo").toArray();

                /*
                 * In case of guest of conference call
                 */
                foreach (const QJsonValue & value, json_array) {
                    QJsonObject obj = value.toObject();
                    if( MATCHED == obj.value("phoneNo").toString().compare(user_number) )  {
                        LOG_DEBUG << " user_number : " << user_number << ", is correct quest in Conference Room";
                        return USER_VALID_OF_CONFCALL;
                    }
                }
            }
        }

        return USER_NOT_VALID_OF_CONFCALL;
    } catch( mongocxx::exception &e ){
        LOG_ERROR << QString("%1").arg(e.what());
        g_mutex.unlock();
        return FAILED;
    }
}


QString
CVoIpServerDataBase::get_mdb_error_to_string(DB_RESULT_E   msg)
{
    QString     ret;
    switch(msg) {
    case DONE                       : ret = "DONE";                 break;
    case NUMBER_VALID               : ret = "Number is VALID";      break;
    case NUMBER_NOT_VALID           : ret = "Number is NOT valid";  break;
    case NUMBER_CONFERENCE          : ret = "Number is CONFERENCE number"; break;
    case NUMBER_NOT_ONLINE          : ret = "Number is NOT online"; break;
    case TEXT_CACHED                : ret = "Number has CACHED text message"; break;
    case TEXT_MULTI_CACHED          : ret = "Number has MULTI CACHED text message"; break;
    case USER_VALID_OF_CONFCALL     : ret = "User is VALID of confcall"; break;
    case USER_NOT_VALID_OF_CONFCALL : ret = "User is NOT valid of confcall"; break;
    case DATABASE_NOT_UPDATED       : ret = "Database has problem, data was NOT updated"; break;
    case NO_DATA_IN_DATABASE        : ret = "There is NO data in database"; break;
    case NOT_VALID_CONFCALL         : ret = "This confence call is NOT valid due to time or was NOT booked..!!"; break;
    default :
        ret = "NONE";
        break;
    }

    return ret;
}
