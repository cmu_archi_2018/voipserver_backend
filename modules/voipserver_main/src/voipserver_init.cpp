#include "voipserver_init.h"

CVoIpServerInit::CVoIpServerInit(QObject *parent) : QObject(parent)
{
    m_command_server_       = nullptr;
    m_keepalive_server_     = nullptr;
    m_database_             = nullptr;
    m_confcall_             = nullptr;

    init();
}

void
CVoIpServerInit::init()
{
    if( nullptr == m_command_server_ ) {
        m_command_server_       = new CVoIpServerListenServer(g_config.m_server_port);
    }
    else {
        LOG_WARN << " CVoIpServerCommandServer ALREADY was made..!!";
    }

    if( nullptr == m_keepalive_server_ ) {
        m_keepalive_server_     = new CVoIpServerListenServer(g_config.m_keepalive_port);
        m_keepalive_server_->set_keepalive();
    }
    else {
        LOG_WARN << " CVoIpServerKeepAliveServer ALREADY was made..!!";
    }

    if( nullptr == m_confcall_ ) {
        m_confcall_             = new CVoIpServerConfCall();
    }
    else {
        LOG_WARN << " CVoIpServerConfCall ALREADY was made..!!";
    }

    if( nullptr == m_database_ ) {
        m_database_         = new CVoIpServerDataBase();
    }
    else {
        LOG_WARN << " CVoIpServerDataBase ALREADY was made..!!";
    }

    if( false == m_command_server_->set_database(m_database_) )     { exit(0); }
    if( false == m_command_server_->set_confcall(m_confcall_) )     { exit(0); }
    if( false == m_keepalive_server_->set_database(m_database_) )   { exit(0); }
    if( false == m_confcall_->set_database(m_database_) )           { exit(0); }

    m_command_server_->start_server(g_config.m_server_port);
    m_keepalive_server_->start_server(g_config.m_keepalive_port);
}
