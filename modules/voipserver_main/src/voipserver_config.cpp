#include <voipserver_config.h>

CVoIpServerConfig::CVoIpServerConfig() {
    m_function              = "SERVER";
    m_server_addr           = "127.0.0.1";
    m_server_port           = 5060;

    m_client_signal_port    = 5060;
    m_client_media_port     = 5064;

    m_message_duration      = 10; // ms

    m_keepalive_port        = 5062;
    m_keepalive_interval    = 15; // min
    m_keepalive_missing     = 3; // Allow missing times.

    m_database_uri          = "mongodb://admin@127.0.0.1:27017";

    m_ssl_certificate       = "rootCA.pem";
    m_ssl_key               = "rootCA.key";
    m_ssl_protocol          = QSsl::UnknownProtocol;

    m_token_secret_key      = "TestKey";
    m_token_issuer          = "team4.cmu.edu";

    m_confcall_bin          = "/app/bin/voipserver_confcall";
    m_ringback_bin          = "/app/bin/voipserver_ringback";

    m_test_callee_addr      = "a";
    m_confcall_start_port   = 10000;
}

bool
CVoIpServerConfig::set_config(QString filepath)
{
    QFile   qconfig(filepath);
    qconfig.open( QIODevice::ReadOnly );

    if( false == qconfig.isOpen() ) {
        LOG_ERROR << "configfile(" << filepath << ") could NOT open..!!";
        return false;
    }

    m_config_file = filepath;

    QByteArray  read_data;
    read_data = qconfig.readAll();

    LOG_DEBUG << " read_data : " << read_data;

    QJsonObject obj    = QJsonDocument::fromJson(read_data.toStdString().c_str()).object();

    m_function                  = obj.value("function").toString();
    m_server_addr               = obj.value("serveraddr").toString();
    m_server_port               = obj.value("serverport").toInt();

    m_client_signal_port        = obj.value("client_signal_port").toInt();
    m_client_media_port         = obj.value("client_media_port").toInt();

    m_message_duration          = obj.value("message_duration").toInt();

    m_keepalive_port            = obj.value("keepalive_port").toInt();
    m_keepalive_interval        = obj.value("keepalive_interval").toInt();
    m_keepalive_missing         = obj.value("keepalive_missing").toInt();

    m_database_uri              = obj.value("database_uri").toString();

    m_ssl_certificate           = obj.value("ssl_certificate").toString();
    m_ssl_key                   = obj.value("ssl_key").toString();
    QString protocol            = obj.value("ssl_protocol").toString();
    m_ssl_protocol              = convert_ssl_protocol(protocol);

    m_token_secret_key          = obj.value("token_secure_key").toString();
    m_token_issuer              = obj.value("token_issuer").toString();

    m_confcall_bin              = obj.value("confcall_bin").toString();
    m_ringback_bin              = obj.value("ringback_bin").toString();

    m_test_callee_addr            = obj.value("testrecvaddr").toString();

    m_confcall_start_port       = obj.value("confcall_start_port").toInt();

    return true;
}

quint16 CVoIpServerConfig::convert_ssl_protocol(QString protocol)
{
    if( MATCHED == protocol.toUpper().compare("SSLV3") ) { return QSsl::SslV3; }
    if( MATCHED == protocol.toUpper().compare("SSLV2") ) { return QSsl::SslV2; }
    if( MATCHED == protocol.toUpper().compare("TLSV1_0") ) { return QSsl::TlsV1_0; }
    if( MATCHED == protocol.toUpper().compare("TLSV1_1") ) { return QSsl::TlsV1_1; }
    if( MATCHED == protocol.toUpper().compare("TLSV1_2") ) { return QSsl::TlsV1_2; }
    if( MATCHED == protocol.toUpper().compare("ANYPROTOCOL") ) { return QSsl::AnyProtocol; }
    if( MATCHED == protocol.toUpper().compare("TLSV1SSLV3") ) { return QSsl::TlsV1SslV3; }
    if( MATCHED == protocol.toUpper().compare("SECUREPROTOCOLS") ) { return QSsl::SecureProtocols; }
    if( MATCHED == protocol.toUpper().compare("TLSV1_0ORLATER") ) { return QSsl::TlsV1_0OrLater; }
    if( MATCHED == protocol.toUpper().compare("TLSV1_1ORLATER") ) { return QSsl::TlsV1_1OrLater; }
    if( MATCHED == protocol.toUpper().compare("TLSV1_2ORLATER") ) { return QSsl::TlsV1_2OrLater; }

    return  QSsl::UnknownProtocol;
}
