#include "voipserver_confcall.h"

CVoIpServerConfCall::CVoIpServerConfCall(QObject *parent) : QObject(parent)
{
    m_database_ = nullptr;
    m_hash_of_conf_call.clear();
    m_hash_of_port_res.clear();

    /*
     * Initialize Port Resources Hash Map
     */

    for( quint16 idx = 0 ; idx < (MAX_CONFCALL_ROOM * MAX_USED_PORT_PER_ROOM) ; idx += MAX_USED_PORT_PER_ROOM ) {
        m_hash_of_port_res.insert(g_config.m_confcall_start_port+idx, NOT_USED);
    }
}

bool
CVoIpServerConfCall::set_database(CVoIpServerDataBase* database_)
{
    if( nullptr == database_ ) {
        LOG_ERROR << " database_ is nullptr...!!!";
        return false;
    }

    m_database_ = database_;
    return true;
}

quint16
CVoIpServerConfCall::ask_to_join( QString conf_number, QJsonObject jsonobj )
{
    QHash<QString,CVoIpServerConfCallWorker*>::iterator             iter;
    QHash<quint16,bool>::iterator                                   iter_ports;

    quint16         ret_val = 0;
    /*
     * Ask to Database this user is valid or not.
     */
    if( USER_VALID_OF_CONFCALL != m_database_->get_valid_user_of_conf_call(jsonobj.value("from").toString(), conf_number ) ) {
        LOG_DEBUG << " This user is NOT valid for this confernece call : " << conf_number;
        return ret_val;
    }

    /*
     * Processing map of confcall
     */
    iter = m_hash_of_conf_call.find(conf_number);

    // found
    if( iter != m_hash_of_conf_call.end() ) {
        ret_val = iter.value()->get_available_port();
    }
    // no found
    else {
        CVoIpServerConfCallWorker* newconfcall = new CVoIpServerConfCallWorker(conf_number);

        if( nullptr == newconfcall ) {
            LOG_ERROR << " CANNOT make Conference Room...!!";
            return ret_val;
        }
        m_hash_of_conf_call.insert(conf_number, newconfcall);

        for( iter_ports = m_hash_of_port_res.begin() ; iter_ports != m_hash_of_port_res.end() ; iter_ports++ ) {
            if( NOT_USED == iter_ports.value() ) {
                newconfcall->set_start_port_num(iter_ports.key());
                iter_ports.value() = USED;
                ret_val = newconfcall->get_available_port();
                break;
            }
        }

        connect( newconfcall,   SIGNAL(signal_destory_me(QString)),  this,   SLOT(slot_destroy_worker(QString)));

        LOG_DEBUG << " Conference RoomNo : " << conf_number.toInt() << " was created..!!";
        LOG_DEBUG << " Total number of Conference Call Worker : " << m_hash_of_conf_call.size();
    }

    return ret_val;
}

bool
CVoIpServerConfCall::join_confcall(QString conf_number, QString client_addr, quint16 client_port )
{
    LOG_DEBUG << " conf_number : " << conf_number << ", is joining...";
    QHash<QString,CVoIpServerConfCallWorker*>::iterator            iter;

    /*
     * Processing map of confcall
     */
    iter = m_hash_of_conf_call.find(conf_number);

    // found
    if( iter != m_hash_of_conf_call.end() ) {
        LOG_DEBUG << " Found confcall number : " << iter.key() << ", joining...";
        return iter.value()->set_add_user(client_addr, client_port);
    }

    LOG_DEBUG << " There is NO available join confcall..!!";
    return false;

}

bool
CVoIpServerConfCall::exit_confcall(QString conf_number, QString client_addr, quint16 client_port )
{
    (void)client_port;
    QHash<QString,CVoIpServerConfCallWorker*>::iterator            iter;

    /*
     * Processing map of confcall
     */
    iter = m_hash_of_conf_call.find(conf_number);

    // found
    if( iter != m_hash_of_conf_call.end() ) {
        LOG_DEBUG << " Found confcall number : " << iter.key() << ", exiting...";
        return iter.value()->set_del_user(client_addr);
    }

    LOG_DEBUG << " There is NO available exit confcall..!!";
    return false;
}

quint16
CVoIpServerConfCall::get_available_port_of_confcall_room(QString conf_number)
{
    QHash<QString,CVoIpServerConfCallWorker*>::iterator            iter;

    /*
     * Processing map of confcall
     */
    iter = m_hash_of_conf_call.find(conf_number);

    // found
    if( iter != m_hash_of_conf_call.end() ) {
        LOG_DEBUG << " Found confcall number : " << iter.key() << ", getting portnumber...";
        return iter.value()->get_available_port();
    }

    LOG_DEBUG << " There is NO available exit confcall..!!";
    return 0;
}

bool
CVoIpServerConfCall::destory_confcall_room(QString conf_number)
{
    QHash<QString,CVoIpServerConfCallWorker*>::iterator            iter;

    /*
     * Processing map of confcall
     */
    iter = m_hash_of_conf_call.find(conf_number);

    // found
    if( iter != m_hash_of_conf_call.end() ) {
        iter.value()->deleteLater();
        return true;
    }

    return false;
}

/*
 * This function will be used internally in this class
 * for checking whether there is available for making new confcall room or not.
 */
quint16
CVoIpServerConfCall::get_available_port()
{
    QHash<quint16,bool>::iterator           iter_ports;
    for( iter_ports = m_hash_of_port_res.begin() ; iter_ports != m_hash_of_port_res.end() ; iter_ports++ ) {
        if( NOT_USED == iter_ports.value() ) {
            return iter_ports.value();
            break;
        }
    }
    return 0;
}

void
CVoIpServerConfCall::slot_destroy_worker(QString    conf_number)
{
    QHash<QString,CVoIpServerConfCallWorker*>::iterator             iter;

    /*
     * Processing map of confcall
     */
    iter = m_hash_of_conf_call.find(conf_number);

    // found
    if( iter != m_hash_of_conf_call.end() ) {
        LOG_DEBUG << " conf_number : " << conf_number << ", destroy";
        iter.value()->deleteLater();
        m_hash_of_conf_call.remove(conf_number);
    }
    // no found
    else {
        LOG_DEBUG << " There is NO conf_number Worker..!!";
    }
}
