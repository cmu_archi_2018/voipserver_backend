#ifndef VOIPSERVER_CONFCALL_LOOP_H
#define VOIPSERVER_CONFCALL_LOOP_H

#include "voipserver_confcall_define.h"

class CVoIpServerConfCallLoop : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerConfCallLoop(QObject *parent = 0);
//    virtual ~CVoIpServerConfCallLoop();

    void            set_main_loop(GMainLoop*);

private :
    GMainLoop*      m_mainloop_;

signals:
    void            signal_exit_main_loop();

public slots:
    void            slot_do_work();
};

#endif // VOIPSERVER_CONFCALL_LOOP_H
