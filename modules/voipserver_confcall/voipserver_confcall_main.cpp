#include <QCoreApplication>
#include <QtDebug>
#include <QFile>
#include <QTextStream>
#include <sys/types.h>
#include <unistd.h>

#include "voipserver_confcall_define.h"
#include "voipserver_confcall_config.h"
#include "voipserver_confcall_init.h"

#include <iostream>

class CVoIpServerConfCallConfig;
CVoIpServerConfCallConfig           g_config;
class CVoIpServerConfCallInit*      g_init;
QString                             filename;

void myMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    (void)context;
    QString txt;
    switch (type) {
    case QtDebugMsg:
        txt = QString("%1").arg(msg);
        break;
    case QtWarningMsg:
        txt = QString("%1").arg(msg);
        break;
    case QtCriticalMsg:
        txt = QString("%1").arg(msg);
        break;
    case QtFatalMsg:
        txt = QString("%1").arg(msg);
        abort();
        break;
    case QtInfoMsg:
    default:
        break;
    }
    QFile outFile("/tmp/"+filename+".log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    std::cout << txt.toStdString() << std::endl;
    ts << txt << endl;
}

void print_help()
{
    std::cout << " Usage : voipserver_confcall [configfile] [mode] [clientaddr] [clientport] [src_port1] [src_port2] [src_port3]" << std::endl;
    std::cout << " [configfile] : default file is on /app/config/config_voipserver.json" << std::endl;
    std::cout << " [mode] : src/sink" << std::endl;
    std::cout << " [clientaddr] : confcall tone would be sended to clientaddr" << std::endl;
    std::cout << " [clientport] : confcall tone would be sended to clientport" << std::endl;
    std::cout << " [src_port] : udpsrc port" << std::endl;
}

void signalHandler(int signal)
{
    //print incoming signal
    switch(signal){
    case SIGINT:  LOG_ERROR << "SIGINT => ";  exit(0);break;
    case SIGKILL: LOG_ERROR << "SIGKILL => "; break;
    case SIGQUIT: LOG_ERROR << "SIGQUIT => "; break;
    case SIGSTOP: LOG_ERROR << "SIGSTOP => "; break;
    case SIGTERM: LOG_ERROR << "SIGTERM => ";
        if( nullptr != g_init )
            g_init->call_terminate_process();
        QThread::usleep(10000);
        exit(0);
        break;
    case SIGSEGV: LOG_ERROR << "SIGSEGV => "; break;
    default: LOG_ERROR << "APPLICATION EXITING => "; break;
    }
}

int main(int argc, char *argv[])
{
    qInstallMessageHandler(myMessageHandler);
    QCoreApplication a(argc, argv);

    QString configfile;
    if( argc < 5 ) {
        print_help();
        exit(0);
    }
    else {
        configfile = argv[1];
    }

    QString function;
    function = argv[2];

    QString clientaddr;
    clientaddr = argv[3];

    g_config.set_config(configfile, clientaddr, function);

    QString clientport;
    clientport = argv[4];
    g_config.m_client_media_port = clientport.toInt();

    filename = clientaddr + "_" + function;

    LOG_INFO << "********************************************************" ;
    LOG_INFO << "***          voipserver_confcall is Started          ***" ;
    LOG_INFO << "***          Process ID = " << a.applicationPid() << "                     ***" ;
    LOG_INFO << "********************************************************" ;

    LOG_DEBUG << " This module has " << g_config.m_function << " function";

    if( MATCHED == g_config.m_function.toUpper().compare("SRC") ) { // MATCHED SRC FUNCTION
        QString listen_port     = argv[5];
        QString sink_socket     = argv[6];

        g_config.m_listen_port  = listen_port.toInt();
        g_config.m_socket_path1 = sink_socket.toInt();
        g_config.print_config();

        g_init = new CVoIpServerConfCallInit(argc, argv);
    }
    else if( MATCHED == g_config.m_function.toUpper().compare("SINK") ) { // MATCHED SINK FUNCTION
        QString bindport        = argv[5];
        QString socket_path1    = argv[6];
        QString socket_path2    = argv[7];
        QString socket_path3    = argv[8];

        g_config.m_bind_port    = bindport.toInt();
        g_config.m_socket_path1 = socket_path1.toInt();
        g_config.m_socket_path2 = socket_path2.toInt();
        g_config.m_socket_path3 = socket_path3.toInt();

        g_config.print_config();
        g_init = new CVoIpServerConfCallInit(argc, argv);
    }
    else {
        LOG_ERROR << "Configuration file has problem, please check it..!!!";
        exit(0);
    }

    struct sigaction act, oact;

    memset((void*)&act,  0, sizeof(struct sigaction));
    memset((void*)&oact, 0, sizeof(struct sigaction));

    act.sa_flags = 0;
    act.sa_handler = &signalHandler;

    sigaction(SIGINT,  &act, &oact);
    sigaction(SIGKILL, &act, &oact);
    sigaction(SIGQUIT, &act, &oact);
    sigaction(SIGSTOP, &act, &oact);
    sigaction(SIGTERM, &act, &oact);

    return a.exec();
}
