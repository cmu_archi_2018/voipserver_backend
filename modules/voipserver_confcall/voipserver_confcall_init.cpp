#include "voipserver_confcall_init.h"

CVoIpServerConfCallInit::CVoIpServerConfCallInit(int argc, char** argv, QObject *parent)
  : QObject(parent),
   m_confcall_core_(nullptr)
{
    if( nullptr == m_confcall_core_ ) {
        m_confcall_core_ = CVoIpServerConfCallCore::get_instance();

        if( nullptr == m_confcall_core_ ) {
            LOG_ERROR << " VoIpServer ConfCall Core class could not be created..!!";
            return;
        }
    }

    if( false == m_confcall_core_->set_configuration(argc, argv) ) {
        LOG_ERROR << " VoIpServer ConfCall Core cannot configured..!!";
        return ;
    }

    connect(this,       SIGNAL(signal_terminate_process()), m_confcall_core_, SLOT(slot_exit_main_loop()));

    QThread         *core_th_ = new QThread();
    connect(core_th_,   SIGNAL(started()),          m_confcall_core_, SLOT(slot_do_work()));
    connect(core_th_,   SIGNAL(finished()),         m_confcall_core_, SLOT(deleteLater()));

    QThread::usleep(100000);

    core_th_->start();
}

void
CVoIpServerConfCallInit::call_terminate_process()
{
    LOG_WARN << " This process will be terminated..!!";
    emit signal_terminate_process();
}

