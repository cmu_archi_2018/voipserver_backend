#ifndef VOIPSERVER_CONFCALL_CORE_H
#define VOIPSERVER_CONFCALL_CORE_H

#include "voipserver_confcall_define.h"
#include "voipserver_confcall_loop.h"
#include "voipserver_confcall_config.h"

extern  CVoIpServerConfCallConfig g_config;
class   CVoIpServerConfCallLoop;

class CVoIpServerConfCallCore : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerConfCallCore(QObject *parent = 0);

    bool     set_configuration(int/*argc*/, char** /*argv[]*/);

    static  CVoIpServerConfCallCore*    get_instance();
    static  CVoIpServerConfCallCore*    m_instance_;

    static gboolean                     bus_message (GstBus* /*bus*/, GstMessage */*message*/, gst_info_t */*app*/);

private:
    gst_info_t*                 m_gst_info_;
    GstBus*                     m_gst_bus_;

    CVoIpServerConfCallLoop*    m_confcall_loop_;
    QString                     m_socket_path;

signals:

public slots:
    void                slot_do_work();
    void                slot_exit_main_loop();
};

#endif // VOIPSERVER_CONFCALL_CORE_H
