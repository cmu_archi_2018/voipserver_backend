#include "voipserver_confcall_loop.h"

CVoIpServerConfCallLoop::CVoIpServerConfCallLoop(QObject *parent) : QObject(parent)
{
    m_mainloop_ = nullptr;
}

void
CVoIpServerConfCallLoop::slot_do_work()
{
    LOG_DEBUG << "CVoIpServerConfCallLoop Thread is started, ThreadID = " << QThread::currentThreadId();

    if( nullptr == m_mainloop_ ) {
        LOG_ERROR << " m_mainloop_ is NULL..!! we cannot execute thread of MainLoop";
        return;
    }

    LOG_INFO << " g_main_loop_run() will be execute..!";

    while (1) {
        LOG_INFO << " Before entering main_loop_run()";
        g_main_loop_run (m_mainloop_);
        LOG_INFO << " After  entering main_loop_run()";
        emit signal_exit_main_loop();
    }
}

void
CVoIpServerConfCallLoop::set_main_loop(GMainLoop *mainloop_)
{
    if( nullptr == mainloop_ ) {
        LOG_ERROR << " mainloop_ is nullptr..!!";
        return;
    }

    m_mainloop_ = mainloop_;
}
