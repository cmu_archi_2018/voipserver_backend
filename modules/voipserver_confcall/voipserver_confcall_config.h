#ifndef VOIPSERVER_CONFCALL_CONFIG_H
#define VOIPSERVER_CONFCALL_CONFIG_H

#include <QObject>
#include <voipserver_confcall_define.h>

class CVoIpServerConfCallConfig : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerConfCallConfig(QObject *parent = 0);
    bool        set_config(QString filepath, QString clientaddr, QString status);
    void        print_config();

    QString     m_function;
    QString     m_client_addr;

    QString     m_ringback_file;
    QString     m_busyback_file;

    QString     m_codec;
    QString     m_encoder;
    QString     m_decoder;

    QString     m_rtp;
    QString     m_rtp_pay;
    QString     m_rtp_depay;

    QString     m_rtp_jitter;

    QString     m_secure;
    QString     m_secure_enc;
    QString     m_secure_dec;
    QString     m_secure_key;

    quint16     m_client_signal_port;
    quint16     m_client_media_port;

    QString     m_src_of_src_caps;
    QString     m_src_of_sink_caps;

    quint16     m_listen_port;
    quint16     m_bind_port;

    quint16     m_socket_path1;
    quint16     m_socket_path2;
    quint16     m_socket_path3;

    QString     m_src_str_no_sec;
    QString     m_src_str_sec;
    QString     m_sink_str_no_sec;
    QString     m_sink_str_sec;

signals:

public slots:
};

#endif // VOIPSERVER_CONFCALL_CONFIG_H
