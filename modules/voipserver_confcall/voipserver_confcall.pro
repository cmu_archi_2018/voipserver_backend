QT += core
QT += dbus
QT -= gui

CONFIG += c++11

TARGET = voipserver_confcall
CONFIG += console
CONFIG -= app_bundle

DESTDIR = /app/bin

INCLUDEPATH += /usr/include/glib-2.0
INCLUDEPATH += /usr/include/gstreamer-1.0
INCLUDEPATH += /usr/lib/glib-2.0/include
INCLUDEPATH += /usr/lib/x86_64-linux-gnu/glib-2.0/include
INCLUDEPATH += /usr/lib/x86_64-linux-gnu/gstreamer-1.0/include

LIBS += -lpthread -lglib-2.0 -lgobject-2.0 -lgstreamer-1.0

TEMPLATE = app

SOURCES += \
    voipserver_confcall_config.cpp \
    voipserver_confcall_core.cpp \
    voipserver_confcall_init.cpp \
    voipserver_confcall_loop.cpp \
    voipserver_confcall_main.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    voipserver_confcall_config.h \
    voipserver_confcall_core.h \
    voipserver_confcall_define.h \
    voipserver_confcall_init.h \
    voipserver_confcall_loop.h
