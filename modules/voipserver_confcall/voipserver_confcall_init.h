#ifndef VOIPSERVER_RINGBACK_INIT_H
#define VOIPSERVER_RINGBACK_INIT_H

#include "voipserver_confcall_define.h"
#include "voipserver_confcall_core.h"

extern CVoIpServerConfCallConfig    g_config;

class CVoIpServerConfCallCore;

class CVoIpServerConfCallInit : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerConfCallInit(int/*argc*/, char**/*argv[]*/, QObject *parent = 0);
    void     call_terminate_process();

private:
    CVoIpServerConfCallCore*    m_confcall_core_;

signals:
    void                        signal_terminate_process();

public slots:
};

#endif // VOIPSERVER_RINGBACK_INIT_H
