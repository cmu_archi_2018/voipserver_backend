#include "voipserver_confcall_core.h"

CVoIpServerConfCallCore*    CVoIpServerConfCallCore::m_instance_     = nullptr;
static gst_info_t           s_gst_info;

CVoIpServerConfCallCore::CVoIpServerConfCallCore(QObject *parent) : QObject(parent)
{
    m_gst_info_      = nullptr;
    m_gst_bus_       = nullptr;
    m_confcall_loop_ = nullptr;
    m_socket_path.clear();
}

CVoIpServerConfCallCore*
CVoIpServerConfCallCore::get_instance()
{
    if( nullptr == m_instance_ ) {
        m_instance_ = new CVoIpServerConfCallCore();
    }

    return m_instance_;
}

void
CVoIpServerConfCallCore::slot_do_work()
{
    LOG_DEBUG << "CVoIpServerConfCallCore Thread is started, ThreadID = " << QThread::currentThreadId() ;

    if( nullptr == m_gst_info_ ) {
        LOG_ERROR << "m_gst_info_ is NULL..!! we cannot execute this thread..!!";
        return;
    }

    if( nullptr == m_confcall_loop_ ) {
        m_confcall_loop_ = new CVoIpServerConfCallLoop();

        if( nullptr == m_confcall_loop_ ) {
            LOG_ERROR << " Cannot create CVoIpServerConfCallLoop class..!!";
            return;
        }
        m_confcall_loop_->set_main_loop(m_gst_info_->loop);
    }

    connect( m_confcall_loop_ ,      SIGNAL(signal_exit_main_loop()), m_instance_, SLOT(slot_exit_main_loop()), Qt::DirectConnection);

    QThread *th_ = new QThread();

    connect(th_, SIGNAL(started()),  m_confcall_loop_, SLOT(slot_do_work()));
    connect(th_, SIGNAL(finished()), m_confcall_loop_, SLOT(deleteLater()));

    // Capture Core Thread Start!!!
    m_confcall_loop_->moveToThread(th_);
    th_->start();
}

bool
CVoIpServerConfCallCore::set_configuration(int argc, char** argv)
{
    /***********************************************************
     * Configuration GStreamer variable
     ***********************************************************/
    m_gst_info_ = &s_gst_info;
    gst_init (&argc, &argv);

    m_gst_info_->loop  = g_main_loop_new( NULL, TRUE );
    g_assert(m_gst_info_->loop);

    /* Create gstreamer elements */
    if( MATCHED == g_config.m_function.toUpper().compare("SRC") ) {
        QString src_str;
        src_str.clear();

        if( MATCHED == g_config.m_secure.size() ) {
            src_str = g_config.m_src_str_no_sec;
            src_str = src_str.arg(g_config.m_rtp_depay).arg(g_config.m_decoder);
        }
        else {
            src_str = g_config.m_src_str_sec;
            src_str = src_str.arg(g_config.m_secure_dec).arg(g_config.m_rtp_depay).arg(g_config.m_decoder);
        }

        LOG_DEBUG << " SRC Pipeline : " << src_str;

        m_gst_info_->pipeline = gst_parse_launch(src_str.toStdString().c_str(), nullptr);
        g_assert( m_gst_info_->pipeline );
    }
    else if( MATCHED == g_config.m_function.toUpper().compare("SINK") ) {
        QString sink_str;
        sink_str.clear();

        if( MATCHED == g_config.m_secure.size() ) {
            sink_str = g_config.m_sink_str_no_sec;
            sink_str = sink_str.arg(g_config.m_socket_path1). \
                     arg(g_config.m_socket_path2). \
                     arg(g_config.m_socket_path3). \
                     arg(g_config.m_encoder).   \
                     arg(g_config.m_rtp_pay);
        }
        else {
            sink_str = g_config.m_sink_str_sec;
            sink_str = sink_str.arg(g_config.m_socket_path1). \
                     arg(g_config.m_socket_path2). \
                     arg(g_config.m_socket_path3). \
                     arg(g_config.m_encoder).   \
                     arg(g_config.m_rtp_pay).   \
                     arg(g_config.m_secure_enc);

        }

        LOG_DEBUG << " SINK Pipeline : " << sink_str;

        m_gst_info_->pipeline = gst_parse_launch(sink_str.toStdString().c_str(), nullptr);
        g_assert( m_gst_info_->pipeline );
    }

    m_gst_bus_ = gst_pipeline_get_bus( GST_PIPELINE( m_gst_info_->pipeline ));
    g_assert( m_gst_bus_ );

    /* add watch for messages */
    gst_bus_add_watch (m_gst_bus_, (GstBusFunc) bus_message, m_gst_info_ );
    if( nullptr != m_gst_bus_ )  gst_object_unref(m_gst_bus_);


    /*
     * Set Properties of source
     */
    if( MATCHED == g_config.m_function.toUpper().compare("SRC") ) {
        m_gst_info_->udpsrc = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "udpsrc" );
        g_assert(m_gst_info_->udpsrc);

        LOG_DEBUG << " SRC udpsrc port : " << g_config.m_listen_port;
        LOG_DEBUG << " SRC udpsrc caps : " << g_config.m_src_of_src_caps;

        g_object_set( G_OBJECT(m_gst_info_->udpsrc),
                        "port",   g_config.m_listen_port,
                        "buffer-size",  2000000,
                        "do-timestamp", true,
                        "caps",   gst_caps_from_string(g_config.m_src_of_src_caps.toStdString().c_str()),
                        nullptr );

        /*
        m_gst_info_->jitter = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "jitter" );
        g_assert(m_gst_info_->jitter);

        LOG_DEBUG << " SRC jitter latency : " << g_config.m_rtp_jitter;

        g_object_set( G_OBJECT(m_gst_info_->jitter),
                        "latency",   g_config.m_rtp_jitter,
                        nullptr );
        */

        m_gst_info_->shmsink = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "sink" );
        g_assert(m_gst_info_->shmsink);

        m_socket_path = "/tmp/team4-" + QString::number(g_config.m_socket_path1);

        LOG_DEBUG << " SRC shmsink socket-path : " << m_socket_path;
        g_object_set( G_OBJECT(m_gst_info_->shmsink),
                      "socket-path",  m_socket_path.toStdString().c_str(),
                      "wait-for-connection", false,
                      "sync", false,
//                      "async", true,
//                      "shmsize", 2000000,
                      nullptr );
    }
    else if( MATCHED == g_config.m_function.toUpper().compare("SINK") ) {

        /* SINK udpsrc Port 1 */
        m_gst_info_->shmsrc1 = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "shmsrc1" );
        g_assert(m_gst_info_->shmsrc1);

        QString     shmsrc1_str = "/tmp/team4-" + QString::number(g_config.m_socket_path1);
//        make_socket(shmsrc1_str);
        LOG_DEBUG << " SINK shmsrc port1 : " << shmsrc1_str;
        g_object_set( G_OBJECT(m_gst_info_->shmsrc1),
                        "socket-path",   shmsrc1_str.toStdString().c_str(),
                        "do-timestamp",  true,
                        "is-live",       true,
//                        "num-buffers",   2000000,
                        nullptr );

        /* SINK shmsrc port 1 */
        m_gst_info_->caps1 = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "caps1" );
        g_assert(m_gst_info_->caps1);

        LOG_DEBUG << " SINK shmsrc caps  : " + g_config.m_src_of_sink_caps;
        g_object_set( G_OBJECT(m_gst_info_->caps1),
                        "caps",   gst_caps_from_string(g_config.m_src_of_sink_caps.toStdString().c_str()),
                        nullptr );

        m_gst_info_->shmsrc2 = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "shmsrc2" );
        g_assert(m_gst_info_->shmsrc2);

        QString     shmsrc2_str = "/tmp/team4-" + QString::number(g_config.m_socket_path2);
        LOG_DEBUG << " SINK shmsrc port2 : " << shmsrc2_str;
//        make_socket(shmsrc2_str);
        g_object_set( G_OBJECT(m_gst_info_->shmsrc2),
                        "socket-path",   shmsrc2_str.toStdString().c_str(),
                        "do-timestamp",  true,
                        "is-live",       true,
//                        "num-buffers",   2000000,
                        nullptr );

        /* SINK shmsrc port 2 */
        m_gst_info_->caps2 = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "caps2" );
        g_assert(m_gst_info_->caps2);

        LOG_DEBUG << " SINK shmsrc caps  : " + g_config.m_src_of_sink_caps;
        g_object_set( G_OBJECT(m_gst_info_->caps2),
                        "caps",   gst_caps_from_string(g_config.m_src_of_sink_caps.toStdString().c_str()),
                        nullptr );

        /* SINK udpsrc Port 3 */
        m_gst_info_->shmsrc3 = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "shmsrc3" );
        g_assert(m_gst_info_->shmsrc3);

        QString     shmsrc3_str = "/tmp/team4-" + QString::number(g_config.m_socket_path3);
        LOG_DEBUG << " SINK shmsrc port3 : " << shmsrc3_str;
//        make_socket(shmsrc3_str);
        g_object_set( G_OBJECT(m_gst_info_->shmsrc3),
                        "socket-path",   shmsrc3_str.toStdString().c_str(),
                        "do-timestamp",  true,
                        "is-live",       true,
//                        "num-buffers",   2000000,
                        nullptr );

        /* SINK shmsrc port 3 */
        m_gst_info_->caps3 = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "caps3" );
        g_assert(m_gst_info_->caps3);

        LOG_DEBUG << " SINK shmsrc caps  : " << g_config.m_src_of_sink_caps;
        g_object_set( G_OBJECT(m_gst_info_->caps3),
                        "caps",   gst_caps_from_string(g_config.m_src_of_sink_caps.toStdString().c_str()),
                        nullptr );

        // for Secure Media Communication
        if( MATCHED != g_config.m_secure.size() ) {
            LOG_WARN << " Set Security Object..!! " << g_config.m_secure_enc;
            m_gst_info_->secure = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "secenc" );
            g_assert(m_gst_info_->secure);

            g_object_set( G_OBJECT(m_gst_info_->secure),
                         "key",   g_config.m_secure_key.toStdString().c_str(),
                          nullptr );
        }

        /* SINK udpsink Host/Port */
        m_gst_info_->udpsink = gst_bin_get_by_name(GST_BIN(m_gst_info_->pipeline), "udpsink" );
        g_assert(m_gst_info_->udpsink);

        LOG_DEBUG << " SINK udpsink Host:Port " << g_config.m_client_addr.toStdString().c_str() << ":" << ", bind_port : " << g_config.m_bind_port << g_config.m_client_media_port;
        g_object_set( G_OBJECT(m_gst_info_->udpsink),
                      "host",  g_config.m_client_addr.toStdString().c_str(),
//                      "bind-port", g_config.m_bind_port,
                      "port",  g_config.m_client_media_port,
                      nullptr );

    }

    m_gst_info_->status = GST_STATUS_NULL;

    LOG_INFO << " m_gst_info_ = " << m_gst_info_;

    if( GST_STATE_CHANGE_FAILURE == gst_element_set_state( m_gst_info_->pipeline, GST_STATE_PLAYING) ) {
        LOG_ERROR << " Failed to change of Gstreamer State (GST_STATE_PLAYING)";
        return false;
    }

    return true;
}

gboolean
CVoIpServerConfCallCore::bus_message (GstBus* /*bus*/, GstMessage * message, gst_info_t *gst_)
{
    switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_ERROR: {
        GError *err = NULL;
        gchar *dbg_info = NULL;

        gst_message_parse_error (message, &err, &dbg_info);
        LOG_ERROR << " Element(" << GST_OBJECT_NAME(message->src) << ") has ERROR (" << err->message <<")";
        LOG_ERROR << " Debug info " << dbg_info;
        g_error_free (err);
        g_free (dbg_info);
        g_main_loop_quit (gst_->loop);
    }
        break;

    case GST_MESSAGE_EOS: {
        LOG_ERROR << " GST_MESSAGE_EOS..!!";
        g_main_loop_quit (gst_->loop);
    }
        break;
    case GST_MESSAGE_STATE_CHANGED: {
        if (GST_MESSAGE_SRC (message) == GST_OBJECT (gst_->pipeline)) {
            GstState old_state, new_state, pending_state;
            gst_message_parse_state_changed (message, &old_state, &new_state, &pending_state);
            LOG_INFO << " GST_MESSAGE_STATE_CHANGED from " << gst_element_state_get_name (old_state) << "(" << old_state << ") to " <<
                        gst_element_state_get_name(new_state) << "(" << new_state << ")";
        }
    }
        break;
    default:
        LOG_INFO << " messagetype = " << GST_MESSAGE_TYPE_NAME(message);
        break;
    }

    return TRUE;
}

void
CVoIpServerConfCallCore::slot_exit_main_loop()
{
    if( MATCHED == g_config.m_function.toUpper().compare("SRC") && false == m_socket_path.isEmpty() ) {
        QFile   socketfile(m_socket_path);
        LOG_DEBUG << " Before exiting main loop, remove socket path file = " << m_socket_path;
        if( socketfile.remove() == true ) LOG_DEBUG << "  SUCCESS..!!";
        else LOG_DEBUG << "  FAILED..!!";
    }

     g_main_loop_quit (m_gst_info_->loop);

     exit(0);
}
