#include "voipserver_confcall_config.h"

CVoIpServerConfCallConfig::CVoIpServerConfCallConfig(QObject *parent) : QObject(parent)
{
    m_function.clear();
    m_client_addr.clear();

    m_ringback_file.clear();
    m_busyback_file.clear();

    m_codec.clear();
    m_encoder.clear();
    m_decoder.clear();

    m_rtp_jitter.clear();

    m_rtp.clear();
    m_rtp_pay.clear();
    m_rtp_depay.clear();

    m_secure.clear();
    m_secure_enc.clear();
    m_secure_dec.clear();
    m_secure_key.clear();

    m_client_signal_port = 0;
    m_client_media_port = 0;

    m_src_of_src_caps.clear();
    m_src_of_sink_caps .clear();

    m_socket_path1 = 0;
    m_socket_path2 = 0;
    m_socket_path3 = 0;

    m_src_str_no_sec.clear();
    m_src_str_sec.clear();
    m_sink_str_no_sec.clear();
    m_sink_str_sec.clear();
}

bool
CVoIpServerConfCallConfig::set_config(QString filepath, QString clientaddr, QString function )
{
    QFile   qconfig(filepath);
    qconfig.open( QIODevice::ReadOnly );

    if( false == qconfig.isOpen() ) {
        LOG_ERROR << "configfile(" << filepath << ") could NOT open..!!";
        return false;
    }

    QByteArray  arr;
    arr = qconfig.readAll();

    LOG_DEBUG << " arr = " << arr;

    QJsonObject obj    = QJsonDocument::fromJson(arr.toStdString().c_str()).object();

    m_function                  = function;
    m_client_signal_port        = obj.value("client_signal_port").toInt();
    m_client_media_port         = obj.value("client_media_port").toInt();

    m_ringback_file             = obj.value("ringback_file").toString();
    m_busyback_file             = obj.value("busyback_file").toString();

    m_secure                    = obj.value("media_secure").toString();
    m_secure_enc                = m_secure + "enc";
    m_secure_dec                = m_secure + "dec";
    m_secure_key                = obj.value("media_secure_key").toString();

    m_codec                     = obj.value("media_codec").toString();
    m_encoder                   = m_codec + "enc";
    m_decoder                   = m_codec + "dec";

    m_rtp_jitter                = obj.value("jitter_ms").toInt();

    m_rtp                       = obj.value("media_rtp").toString();
    m_rtp_pay                   = "rtp" + m_rtp + "pay";
    m_rtp_depay                 = "rtp" + m_rtp + "depay";

    m_src_of_src_caps           = obj.value("src_of_src_caps").toString();
    m_src_of_sink_caps          = obj.value("src_of_sink_caps").toString();

    m_client_addr               = clientaddr;

//    m_src_str_no_sec            = "udpsrc name=udpsrc ! rtpjitterbuffer name=jitter ! %1 name=rtpdepay ! %2 name=decoder ! audioconvert ! shmsink name=sink";
    m_src_str_no_sec            = "udpsrc name=udpsrc ! %1 name=rtpdepay ! %2 name=decoder ! audioconvert ! shmsink name=sink";
    m_sink_str_no_sec           = "audiomixer name=mix mix. ! %4 name=encoder ! %5 name=rtppay ! udpsink name=udpsink  shmsrc socket-path=%1 name=shmsrc1 ! capsfilter name=caps1 ! mix. shmsrc socket-path=%2 name=shmsrc2 ! capsfilter name=caps2 ! mix. shmsrc socket-path=%3 name=shmsrc3 ! capsfilter name=caps3 ! mix. ";

    m_src_str_sec               = "udpsrc name=udpsrc ! %1 name=secdec ! rtpjitterbuffer name=jitter ! %2 name=rtpdepay ! %3 name=decoder ! audioconvert ! udpsink name=udpsink";
    m_sink_str_sec              = "audiomixer name=mix mix. ! %4 name=encoder ! %5 name=rtppay ! %6 name=secenc ! udpsink name=udpsink  udpsrc port=%1 name=udpsrc ! mix. udpsrc port=%2 name=udpsrc2 ! mix. udpsrc port=%3 name=udpsrc3 ! mix. ";

    return true;
}

void
CVoIpServerConfCallConfig::print_config()
{
    LOG_INFO << " + Configuration is below~~ ";
    LOG_INFO << "   - m_function                  : " << m_function;
    LOG_INFO << "   - m_client_addr               : " << m_client_addr;

    LOG_INFO << "   - m_client_signal_port        : " << m_client_signal_port;
    LOG_INFO << "   - m_client_media_port         : " << m_client_media_port;

    LOG_INFO << "   - m_ringback_file             : " << m_ringback_file;
    LOG_INFO << "   - m_busyback_file             : " << m_busyback_file;

    LOG_INFO << "   - m_media_codec               : " << m_codec;
    LOG_INFO << "     + encoder                   : " << m_encoder;
    LOG_INFO << "     + decoder                   : " << m_decoder;

    LOG_INFO << "   - m_rtp_jitter(ms)            : " << m_rtp_jitter;

    LOG_INFO << "   - m_media_rtp                 : " << m_rtp;
    LOG_INFO << "     + payload                   : " << m_rtp_pay;
    LOG_INFO << "     + depayload                 : " << m_rtp_depay;

    LOG_INFO << "   - m_media_secure              : " << m_secure;
    LOG_INFO << "     + secure_enc                : " << m_secure_enc;
    LOG_INFO << "     + secure_dec                : " << m_secure_dec;

    LOG_INFO << "   - m_socket_path1              : " << m_socket_path1;
    LOG_INFO << "   - m_socket_path2              : " << m_socket_path2;
    LOG_INFO << "   - m_socket_path3              : " << m_socket_path3;
}
