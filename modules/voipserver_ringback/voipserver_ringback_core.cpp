#include "voipserver_ringback_core.h"

CVoIpServerRingBackCore*    CVoIpServerRingBackCore::m_instance_     = nullptr;
//static cb_info_t            s_cb_info;
static gst_info_t           s_gst_info;

CVoIpServerRingBackCore::CVoIpServerRingBackCore(QObject *parent) : QObject(parent)
{
    m_gst_info_     = nullptr;
//    m_pipeline_str.clear();

    m_gst_bus_      = nullptr;
//    m_gst_caps_     = nullptr;

    m_ringback_loop_ = nullptr;
}

CVoIpServerRingBackCore*
CVoIpServerRingBackCore::get_instance()
{
    if( nullptr == m_instance_ ) {
        m_instance_ = new CVoIpServerRingBackCore();
    }

    return m_instance_;
}

void
CVoIpServerRingBackCore::slot_do_work()
{
    LOG_DEBUG << "CVoIpServerRingBackCore Thread is started, ThreadID = " << QThread::currentThreadId() ;

    if( nullptr == m_gst_info_ ) {
        LOG_ERROR << "m_gst_info_ is NULL..!! we cannot execute this thread..!!";
        return;
    }

    if( nullptr == m_ringback_loop_ ) {
        m_ringback_loop_ = new CVoIpServerRingBackLoop();

        if( nullptr == m_ringback_loop_ ) {
            LOG_ERROR << " Cannot create CVoIpServerRingBackLoop class..!!";
            return;
        }
        m_ringback_loop_->set_main_loop(m_gst_info_->loop);
    }

    connect( m_ringback_loop_,      SIGNAL(signal_exit_main_loop()), m_instance_, SLOT(slot_exit_main_loop()), Qt::DirectConnection);

    QThread *th_ = new QThread();

    connect(th_, SIGNAL(started()),  m_ringback_loop_, SLOT(slot_do_work()));
    connect(th_, SIGNAL(finished()), m_ringback_loop_, SLOT(deleteLater()));

    // Capture Core Thread Start!!!
    m_ringback_loop_->moveToThread(th_);
    th_->start();
}

bool
CVoIpServerRingBackCore::set_configuration(int argc, char** argv)
{
    /***********************************************************
     * Configuration GStreamer variable
     ***********************************************************/
    m_gst_info_ = &s_gst_info;
    gst_init (&argc, &argv);

    m_gst_info_->loop  = g_main_loop_new( NULL, TRUE );
    g_assert(m_gst_info_->loop);

    /* Create gstreamer elements */
    m_gst_info_->pipeline = gst_pipeline_new("ringtone-player");                                g_assert( m_gst_info_->pipeline );
    m_gst_info_->filesrc  = gst_element_factory_make ("filesrc",            "file-source");     g_assert( m_gst_info_->filesrc );
    m_gst_info_->parser   = gst_element_factory_make ("wavparse",           "wave-parser");     g_assert( m_gst_info_->parser );
    m_gst_info_->convertor= gst_element_factory_make ("audioconvert",       "audio-convert");   g_assert( m_gst_info_->convertor );
    m_gst_info_->resample = gst_element_factory_make ("audioresample",      "audio-resample");  g_assert( m_gst_info_->resample );
    m_gst_info_->codec    = gst_element_factory_make (g_config.m_encoder.toStdString().c_str(),       "codec");         g_assert( m_gst_info_->codec );
    m_gst_info_->rtppay   = gst_element_factory_make (g_config.m_rtp_pay.toStdString().c_str(),      "rtp-payload");   g_assert( m_gst_info_->rtppay );

    if( 0 != g_config.m_secure.size()) {
        m_gst_info_->secure = gst_element_factory_make (g_config.m_secure_enc.toStdString().c_str(),      "secure-rtp" );     g_assert( m_gst_info_->secure );
    }
    else {
        m_gst_info_->secure = nullptr;
    }
    m_gst_info_->sink     = gst_element_factory_make ("udpsink",            "udp-sink");        g_assert( m_gst_info_->sink );

    /* we set the input filename to the source element */
    if ( MATCHED == g_config.m_status.toUpper().compare("BUSY") )
        g_object_set (G_OBJECT (m_gst_info_->filesrc), "location", g_config.m_busyback_file.toStdString().c_str(), nullptr);
    else
        g_object_set (G_OBJECT (m_gst_info_->filesrc), "location", g_config.m_ringback_file.toStdString().c_str(), nullptr);


    g_object_set (G_OBJECT (m_gst_info_->sink), "host", g_config.m_client_addr.toStdString().c_str(), nullptr);
    g_object_set (G_OBJECT (m_gst_info_->sink), "port", g_config.m_client_media_port, nullptr);

    m_gst_bus_ = gst_pipeline_get_bus( GST_PIPELINE( m_gst_info_->pipeline ));
    g_assert( m_gst_bus_ );

    /* add watch for messages */
    gst_bus_add_watch (m_gst_bus_, (GstBusFunc) bus_message, m_gst_info_ );
    if( nullptr != m_gst_bus_ )  gst_object_unref(m_gst_bus_);

    if( 0 == g_config.m_secure.size() ) {
        gst_bin_add_many ( GST_BIN (m_gst_info_->pipeline),
                           m_gst_info_->filesrc,
                           m_gst_info_->parser,
                           m_gst_info_->convertor,
                           m_gst_info_->resample,
                           m_gst_info_->codec,
                           m_gst_info_->rtppay,
                           m_gst_info_->sink, nullptr );

        gst_element_link_many ( m_gst_info_->filesrc,
                                m_gst_info_->parser,
                                m_gst_info_->convertor,
                                m_gst_info_->resample,
                                m_gst_info_->codec,
                                m_gst_info_->rtppay,
                                m_gst_info_->sink, nullptr);
    }
    else {
        gst_bin_add_many ( GST_BIN (m_gst_info_->pipeline),
                           m_gst_info_->filesrc,
                           m_gst_info_->parser,
                           m_gst_info_->convertor,
                           m_gst_info_->resample,
                           m_gst_info_->codec,
                           m_gst_info_->rtppay,
                           m_gst_info_->secure,
                           m_gst_info_->sink, nullptr );

        gst_element_link_many ( m_gst_info_->convertor,
                                m_gst_info_->resample,
                                m_gst_info_->codec,
                                m_gst_info_->rtppay,
                                m_gst_info_->secure,
                                m_gst_info_->sink, nullptr);
    }

    m_gst_info_->status = GST_STATUS_NULL;
//    pthread_mutex_init(&m_gst_info_->str_lock, nullptr);
//    pthread_cond_init(&m_gst_info_->str_cond, nullptr);

    LOG_INFO << " m_gst_info_ = " << m_gst_info_;

    if( GST_STATE_CHANGE_FAILURE == gst_element_set_state( m_gst_info_->pipeline, GST_STATE_PLAYING) ) {
        LOG_ERROR << " Failed to change of Gstreamer State (GST_STATE_PLAYING)";
        return false;
    }

    return true;
}

gboolean
CVoIpServerRingBackCore::bus_message (GstBus* /*bus*/, GstMessage * message, gst_info_t *gst_)
{
    switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_ERROR: {
        GError *err = NULL;
        gchar *dbg_info = NULL;

        gst_message_parse_error (message, &err, &dbg_info);
        LOG_ERROR << " Element(" << GST_OBJECT_NAME(message->src) << ") has ERROR (" << err->message <<")";
        LOG_ERROR << " Debug info " << dbg_info;
        g_error_free (err);
        g_free (dbg_info);
        g_main_loop_quit (gst_->loop);
    }
        break;

    case GST_MESSAGE_EOS: {
        LOG_ERROR << " GST_MESSAGE_EOS..!!";
        /* restart playback if at end */
        if (!gst_element_seek(gst_->pipeline,
                    1.0, GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH,
                    GST_SEEK_TYPE_SET, 0,
                    GST_SEEK_TYPE_NONE, GST_CLOCK_TIME_NONE)) {
            g_print("Seek failed!\n");
            g_main_loop_quit (gst_->loop);
        }
    }
        break;
    case GST_MESSAGE_STATE_CHANGED: {
        if (GST_MESSAGE_SRC (message) == GST_OBJECT (gst_->pipeline)) {
            GstState old_state, new_state, pending_state;
            gst_message_parse_state_changed (message, &old_state, &new_state, &pending_state);
            LOG_INFO << " GST_MESSAGE_STATE_CHANGED from " << gst_element_state_get_name (old_state) << "(" << old_state << ") to " <<
                        gst_element_state_get_name(new_state) << "(" << new_state << ")";
        }
    }
        break;
    default:
        LOG_INFO << " messagetype = " << GST_MESSAGE_TYPE_NAME(message);
        break;
    }

    return TRUE;
}

void
CVoIpServerRingBackCore::slot_exit_main_loop()
{
     g_main_loop_quit (m_gst_info_->loop);
#if 0
//    int ret = 0;
    if( GST_STATUS_PAUSE_REQ == m_gst_info_->status ) {
        gst_pause_work(m_gst_info_);

        /* Wait RESUME REQ Status */
        LOG_INFO << " Set GST_STATUS_PAUSE_RSP. Wait GST_STATUS_RESUME_REQ";
        ret = gst_str_set_statussig_and_wait( m_gst_info_, GST_STATUS_PAUSE_RSP, 1, GST_STATUS_RESUME_REQ, -1);
        if( ret ) {
            LOG_ERROR << " app_str_set_statussig_and_wait failed(GST_STATUS_PAUSE_RSP, rc=" << ret << ")";
        }

        gst_resume_work( m_gst_info_ );

//        LOG_INFO << "Set GST_STATUS_RESUME_RSP";
//        ret = APP_STR_SET_STATUSSIG(m_pAppInfo, GST_STATUS_RESUME_RSP);

//        if( ret ) {
//            LOG_ERROR << " app_str_set_status failed(GST_STATUS_RESUME_RSP, rc=" << ret << ")";
//        }
//    }
#endif
}



#if 0
    /* we add a message handler */
    bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
    bus_watch_id = gst_bus_add_watch (bus, bus_call, loop);
    gst_object_unref (bus);

    /* we add all elements into the pipeline */
    /* file-source | ogg-demuxer | vorbis-decoder | converter | alsa-output */
    gst_bin_add_many (GST_BIN (pipeline),
                      source, demuxer, decoder, conv, sink, NULL);

    /* we link the elements together */
    /* file-source -> ogg-demuxer ~> vorbis-decoder -> converter -> alsa-output */
    gst_element_link (source, demuxer);
    gst_element_link_many (decoder, conv, sink, NULL);
    g_signal_connect (demuxer, "pad-added", G_CALLBACK (on_pad_added), decoder);

    /* note that the demuxer will be linked to the decoder dynamically.
       The reason is that Ogg may contain various streams (for example
       audio and video). The source pad(s) will be created at run time,
       by the demuxer when it detects the amount and nature of streams.
       Therefore we connect a callback function which will be executed
       when the "pad-added" is emitted.*/

    m_sLaunchStr = QString(LAUNCH_STRING).arg(m_gst_info->width).arg(m_gst_info->height).arg(argv[4]);
    m_gst_info->pipeline = gst_parse_launch(m_sLaunchStr.toStdString().c_str(), NULL);
    g_assert( m_gst_info->pipeline );


    m_pGstBus = gst_pipeline_get_bus( GST_PIPELINE( m_gst_info->pipeline ));
    g_assert( m_pGstBus );

    /* add watch for messages */
    gst_bus_add_watch (m_pGstBus, (GstBusFunc) bus_message, m_gst_info );
    if( m_pGstBus != NULL )  gst_object_unref(m_pGstBus);

    /* get the appsrc */
    m_gst_info->appsrc = gst_bin_get_by_name (GST_BIN(m_gst_info->pipeline), "drmwbsrc");
    g_assert(m_gst_info->appsrc);
    g_assert(GST_IS_APP_SRC(m_gst_info->appsrc));

    g_object_set (G_OBJECT (m_gst_info->appsrc),
                    "stream-type",  0,
                    "is-live",      TRUE,
                    "blocksize",    4096,
                    "do-timestamp", TRUE,
                    "format",       GST_FORMAT_TIME,
                    NULL);

    g_signal_connect (m_gst_info->appsrc, "need-data",   G_CALLBACK (start_feed), m_gst_info);
    g_signal_connect (m_gst_info->appsrc, "enough-data", G_CALLBACK (stop_feed),  m_gst_info);

    /* set the caps on the source */
    m_pGstCaps = gst_caps_new_simple( "video/x-raw",
                                "format",    G_TYPE_STRING, "NV12",
                                "framerate", GST_TYPE_FRACTION, 30, 1,
                                "bpp",       G_TYPE_INT, 24,
                                "depth",     G_TYPE_INT, 24,
                                "width",     G_TYPE_INT, m_gst_info->width,
                                "height",    G_TYPE_INT, m_gst_info->height,
                                NULL );

    gst_app_src_set_caps(GST_APP_SRC(m_gst_info->appsrc), m_pGstCaps);

    /*
    m_pGstOmxEnc = gst_bin_get_by_name(GST_BIN(m_gst_info->pipeline), "omxenc" );
    g_assert(m_pGstOmxEnc);

    g_object_set( G_OBJECT (m_pGstOmxEnc),
                    "control-rate",     2,
                    "periodicty-idr",   1,
                    "target-bitrate",   11485760,
                    "interval_intraframes", 10,
                    "use-dmabuf", TRUE, NULL );
    */

    m_gst_info->omxh264enc = gst_bin_get_by_name(GST_BIN(m_gst_info->pipeline), "omxenc" );
    g_assert(m_gst_info->omxh264enc);

    omxenc_object_set(m_gst_info);

    for( int idx = 0 ; idx < BUF_NUM ; idx++ ) {
        m_gst_info->mmngr_alloc_ret = 0;
        m_gst_info->mmngr_alloc_ret |= mmngr_alloc_in_user_ext(&m_gst_info->conv_pid[idx],
                                                        m_gst_info->width*m_gst_info->height*3/2,
                                                        &m_gst_info->conv_phy_addr[idx],
                                                        &m_gst_info->conv_log_addr[idx],
                                                        MMNGR_VA_SUPPORT, NULL);

        if( m_gst_info->mmngr_alloc_ret != R_MM_OK){
            LOG_ERROR << " Failed to allocate mmngr_alloc_ret";
            LOG_FUNC_END;
            return false;
        }

        ret = mmngr_export_start_in_user_ext(&m_gst_info->conv_dmabuf_pid[idx],
                                        ALIGN_4K_SIZE(m_gst_info->width*m_gst_info->height*3/2),
                                        m_gst_info->conv_phy_addr[idx],
                                        &m_gst_info->dmabuf_fd[idx],
                                        NULL );

        if( ret != R_MM_OK ) {
            LOG_ERROR << " Failed to start of mmngr_export_start_in_user_ext fail(idx=" << idx << ")";
            LOG_FUNC_END;
            return false;
        }

        m_gst_info->gstmem[idx] = gst_dmabuf_allocator_alloc (m_gst_info->allocator,
                                                              m_gst_info->dmabuf_fd[idx],
                                                              ALIGN_4K_SIZE(m_gst_info->width*m_gst_info->height*3/2));

        g_assert(m_gst_info->gstmem[idx]);

    }

    m_gst_info->status = GST_STATUS_NULL;
    pthread_mutex_init(&m_gst_info->str_lock, NULL);
    pthread_cond_init(&m_gst_info->str_cond, NULL);

    m_gst_info->src_caps_ele = gst_bin_get_by_name (GST_BIN(m_gst_info->pipeline), "capsfilter0");
    g_assert(m_gst_info->src_caps_ele);

    m_gst_info->sink_caps_ele = gst_bin_get_by_name (GST_BIN(m_gst_info->pipeline), "capsfilter1");
    g_assert(m_gst_info->sink_caps_ele);

    sem_init(&m_gst_info->dmabuf_sem, 0x00, BUF_NUM);

    ret = vspm_init();
    if (ret != R_VSPM_OK) {
        LOG_ERROR << " Failed to vspm_init_driver(), error code =" << ret;
        LOG_FUNC_END;
        return false;
    }

    m_gst_info->drm_fd = drmOpen("rcar-du", NULL);
    if(m_gst_info->drm_fd < 0) {
        LOG_ERROR << " Failed to open drmOpen(), drm_fd" << m_gst_info->drm_fd;
        LOG_FUNC_END;
        return false;
    }
    else {
        LOG_INFO << " m_gst_info = " << m_gst_info;
        if( gst_element_set_state( m_gst_info->pipeline, GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE ) {
            LOG_ERROR << " Failed to change of Gstreamer State (GST_STATE_PLAYING)";
            LOG_FUNC_END;
            return false;
        }
    }

    LOG_FUNC_END;
    return true;
}

//int
//CVoIpServerRingBackCore::gst_str_set_statussig_and_wait(gst_info_t *app, GST_STATUS_E set_status, int condsig, GST_STATUS_E wait_status, int tm_msec)
//{
//    int ret = 0;
//    struct timespec tm;
//    pthread_mutex_t *lock = NULL;
//    pthread_cond_t 	*cond;

//    lock = &app->str_lock;
//    cond = &app->str_cond;

//    pthread_mutex_lock(lock);
//    app->status=set_status;

//    if( condsig ) pthread_cond_signal(cond);

//    if( wait_status != GST_STATUS_NULL ) {
//        if( tm_msec < 0 ) {
//            ret = pthread_cond_wait(cond, lock);
//        }
//        else {
//            TIME_SET( &tm, tm_msec );
//            ret = pthread_cond_timedwait(cond, lock, &tm);
//        }

//        if( app->status == wait_status )
//            ret = 0;
//        else
//            ret = -1;
//    }
//    pthread_mutex_unlock(lock);
//    return ret;
//}

//GST_STATUS_E
//CVoIpServerRingBackCore::gst_str_get_status(gst_info_t *app)
//{
//    pthread_mutex_t *lock = NULL;
//    GST_STATUS_E      status;

//    lock = &app->str_lock;

//    pthread_mutex_lock(lock);
//    status=app->status;
//    pthread_mutex_unlock(lock);

//    return status;
//}
#endif
