#ifndef VOIPSERVER_RINGBACK_DEFINE_H
#define VOIPSERVER_RINGBACK_DEFINE_H

#include <QtGlobal>

#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusArgument>
#include <QDBusMetaType>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

#include <QThread>

#include <unistd.h>
#include <semaphore.h>


#include <QDebug>
#include <memory>

#include <gst/gst.h>
#include <gst/gstbuffer.h>
#include <pthread.h>

#include "voipserver_ringback_config.h"

const quint16           NOT_CONNECTED = 0;
const quint16           MATCHED = 0;

#ifndef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

#define LOG_PRE     qDebug().noquote() << QString("[%1, %2]").arg(__FILENAME__, -40).arg(__LINE__, 5)

#define LOG_INFO    LOG_PRE << " : [I]"
#define LOG_WARN    LOG_PRE << " : [W]"
#define LOG_ERROR   LOG_PRE << " : [E]"
#define LOG_DEBUG   LOG_PRE << " : [D]"

/*******************************************************
 * Define Enums
 *******************************************************/
/* Application status for STR */
typedef enum {
    GST_STATUS_NULL         = 0,
    GST_STATUS_PAUSE_REQ,
    GST_STATUS_PAUSE_RSP,
    GST_STATUS_RESUME_REQ,
    GST_STATUS_RESUME_RSP,
}GST_STATUS_E;

/*******************************************************
 * Define Structs
 *******************************************************/
typedef struct _gst_info_t
{
    GstElement      *pipeline;
    GstElement      *filesrc;
    GstElement      *parser;
    GstElement      *convertor;
    GstElement      *resample;
    GstElement      *codec;
    GstElement      *secure;
    GstElement      *rtppay;
    GstElement      *sink;

    GMainLoop       *loop;

    GST_STATUS_E	status;

    bool            dbus;

//    int             stm;
//    guint           sourceid;
//    GstElement      *src_caps_ele;
//    GstElement      *sink_caps_ele;
//    pthread_mutex_t str_lock;
//    pthread_cond_t 	str_cond;
} gst_info_t;

//typedef struct _cb_info_t
//{
//    unsigned long   job_id;
//    long            ret;

//    pthread_mutex_t mutex;
//    pthread_cond_t  cond;
//} cb_info_t;


#endif // VOIPSERVER_RINGBACK_DEFINE_H
