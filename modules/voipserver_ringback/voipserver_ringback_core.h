#ifndef VOIPSERVER_RINGBACK_CORE_H
#define VOIPSERVER_RINGBACK_CORE_H

#include <voipserver_ringback_define.h>
#include <voipserver_ringback_loop.h>
#include "voipserver_ringback_config.h"

extern  CVoIpServerRingBackConfig g_config;
class CVoIpServerRingBackLoop;

class CVoIpServerRingBackCore : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerRingBackCore(QObject *parent = 0);
//    virtual ~CVoIpServerRingBackCore();

    bool     set_configuration(int/*argc*/, char** /*argv[]*/);

    static  CVoIpServerRingBackCore*    get_instance();
    static  CVoIpServerRingBackCore*    m_instance_;

//    static int          get_timems(void);
//    static void         free_data(gpointer/*param*/);
//    static void         gst_buffer_memory_unref (gst_info_t*);
//    static GQuark       gst_buffer_pool_import_quark(void);
//    static int          gst_str_set_statussig_and_wait(gst_info_t */*app*/, GST_STATUS_E /*set_status*/, int /*condsig*/, GST_STATUS_E /*wait_status*/, int /*tm_msec*/);
//    static GST_STATUS_E gst_str_get_status(gst_info_t */*app*/);
//    static int          gst_pause(gst_info_t* /*app*/);
//    static int          gst_resume(gst_info_t* /*app*/);
//    static int          gst_pause_work(gst_info_t* /*app*/);
//    static int          gst_resume_work(gst_info_t* /*app*/);
//    static gboolean     read_data(app_info_t*);
//    static void         start_feed(GstElement *, guint, gst_info_t*);
//    static void         stop_feed(GstElement *, gst_info_t*);
    static gboolean     bus_message (GstBus* /*bus*/, GstMessage */*message*/, gst_info_t */*app*/);

private:
    gst_info_t*         m_gst_info_;
//    QString             m_pipeline_str;

    GstBus*             m_gst_bus_;
//    GstCaps*            m_gst_caps_;

    CVoIpServerRingBackLoop*    m_ringback_loop_;

signals:

public slots:
    void                slot_do_work();
//    void                slot_set_status(bool);
    void                slot_exit_main_loop();
};

#endif // VOIPSERVER_RINGBACK_CORE_H
