#include "voipserver_ringback_init.h"

CVoIpServerRingBackInit::CVoIpServerRingBackInit(int argc, char** argv, QObject *parent)
  : QObject(parent),
   m_ringback_core_(nullptr)
{
    if( nullptr == m_ringback_core_ ) {
        m_ringback_core_ = CVoIpServerRingBackCore::get_instance();

        if( nullptr == m_ringback_core_ ) {
            LOG_ERROR << " VoIpServer RingBack Core class could not be created..!!";
            return;
        }
    }

    if( m_ringback_core_->set_configuration(argc, argv) == false ) {
        LOG_ERROR << " VoIpServer RingBack Core cannot configured..!!";
        return ;
    }

    QThread         *core_th_ = new QThread();
    connect(core_th_,   SIGNAL(started()),          m_ringback_core_, SLOT(slot_do_work()));
    connect(core_th_,   SIGNAL(finished()),         m_ringback_core_, SLOT(deleteLater()));

    core_th_->start();
}
