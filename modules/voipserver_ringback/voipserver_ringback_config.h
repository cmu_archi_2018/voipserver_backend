#ifndef VOIPSERVER_RINGBACK_CONFIG_H
#define VOIPSERVER_RINGBACK_CONFIG_H

#include <QObject>
#include <voipserver_ringback_define.h>

class CVoIpServerRingBackConfig : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerRingBackConfig(QObject *parent = 0);
    bool        set_config(QString filepath, QString clientaddr, QString status);
    void        print_config();

    QString     m_status;

    QString     m_function;
    QString     m_client_addr;
    QString     m_client_port;

    QString     m_ringback_file;
    QString     m_busyback_file;

    QString     m_codec;
    QString     m_encoder;

    QString     m_rtp;
    QString     m_rtp_pay;

    QString     m_secure;
    QString     m_secure_enc;

    quint16     m_client_signal_port;
    quint16     m_client_media_port;

signals:

public slots:
};

#endif // VOIPSERVER_RINGBACK_CONFIG_H
