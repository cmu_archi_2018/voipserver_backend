#include <QCoreApplication>
#include <voipserver_ringback_define.h>
#include <voipserver_ringback_config.h>
#include <voipserver_ringback_init.h>

#include <iostream>

class CVoIpServerRingBackConfig;
CVoIpServerRingBackConfig   g_config;

void print_help()
{
    std::cout << " Usage : voipserver_ringback [configfile] [clientaddr] [port] [mode]" << std::endl;
    std::cout << " [configfile] : default file is on /app/config/config_voipserver.json" << std::endl;
    std::cout << " [clientaddr] : ringback tone would be sended to clientaddr by configuration file port" << std::endl;
    std::cout << " [type] : ring/busy" << std::endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString configfile;
    if( argc != 4 ) {
        print_help();
        configfile = "/app/config/config_voipserver.json";
    }
    else {
        configfile = argv[1];
    }

    QString clientaddr;
    clientaddr = argv[2];

    QString status;
    status = argv[3];

    g_config.set_config(configfile, clientaddr, status);

    LOG_INFO << "********************************************************" ;
    LOG_INFO << "***          voipserverringback is Started               ***" ;
    LOG_INFO << "***          Process ID = " << a.applicationPid() << "                     ***" ;
    LOG_INFO << "********************************************************" ;

    if( MATCHED == g_config.m_function.toUpper().compare("SERVER") ) { // MATCHED SERVER FUNCTION
        g_config.print_config();
        new CVoIpServerRingBackInit(argc, argv);
    }
    else {
        LOG_ERROR << "Configuration file has problem, please check it..!!!";
        exit(0);
    }

    return a.exec();
}
