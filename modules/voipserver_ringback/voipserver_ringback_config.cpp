#include "voipserver_ringback_config.h"

CVoIpServerRingBackConfig::CVoIpServerRingBackConfig(QObject *parent) : QObject(parent)
{
}

bool
CVoIpServerRingBackConfig::set_config(QString filepath, QString clientaddr, QString status )
{
    QFile   qconfig(filepath);
    qconfig.open( QIODevice::ReadOnly );

    if( false == qconfig.isOpen() ) {
        LOG_ERROR << "configfile(" << filepath << ") could NOT open..!!";
        return false;
    }

    QByteArray  arr;
    arr = qconfig.readAll();

    LOG_DEBUG << " arr = " << arr;

    QJsonObject obj    = QJsonDocument::fromJson(arr.toStdString().c_str()).object();

    m_function                  = obj.value("function").toString();
    m_client_signal_port        = obj.value("client_signal_port").toInt();
    m_client_media_port         = obj.value("client_media_port").toInt();

    m_ringback_file             = obj.value("ringback_file").toString();
    m_busyback_file             = obj.value("busyback_file").toString();

    m_secure                    = obj.value("media_secure").toString();
    m_secure_enc                = m_secure + "enc";

    m_codec                     = obj.value("media_codec").toString();
    m_encoder                   = m_codec + "enc";

    m_rtp                       = obj.value("media_rtp").toString();
    m_rtp_pay                   = "rtp" + m_rtp + "pay";

    m_client_addr               = clientaddr;
    m_status                    = status;

    return true;
}

void
CVoIpServerRingBackConfig::print_config()
{
    LOG_INFO << " + Configuration is below~~ ";
    LOG_INFO << "   - m_client_signal_port        : " << m_client_signal_port;
    LOG_INFO << "   - m_client_media_port         : " << m_client_media_port;
    LOG_INFO << "   - m_ringback_file             : " << m_ringback_file;
    LOG_INFO << "   - m_busyback_file             : " << m_busyback_file;
    LOG_INFO << "   - m_media_codec               : " << m_encoder;
    LOG_INFO << "   - m_media_payload             : " << m_rtp_pay;
    LOG_INFO << "   - m_media_secure              : " << m_secure_enc;
    LOG_INFO << "   - m_client_addr               : " << m_client_addr;
    LOG_INFO << "   - m_status                    : " << m_status;
}
