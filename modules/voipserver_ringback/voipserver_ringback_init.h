#ifndef VOIPSERVER_RINGBACK_INIT_H
#define VOIPSERVER_RINGBACK_INIT_H

#include <voipserver_ringback_define.h>
#include <voipserver_ringback_core.h>

extern CVoIpServerRingBackConfig    g_config;

class CVoIpServerRingBackCore;

class CVoIpServerRingBackInit : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerRingBackInit(int/*argc*/, char**/*argv[]*/, QObject *parent = 0);

private:
    CVoIpServerRingBackCore*    m_ringback_core_;

signals:

public slots:
};

#endif // VOIPSERVER_RINGBACK_INIT_H
