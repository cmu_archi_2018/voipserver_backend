#ifndef VOIPSERVER_RINGBACK_LOOP_H
#define VOIPSERVER_RINGBACK_LOOP_H

#include "voipserver_ringback_define.h"

class CVoIpServerRingBackLoop : public QObject
{
    Q_OBJECT
public:
    explicit CVoIpServerRingBackLoop(QObject *parent = 0);
//    virtual ~CVoIpServerRingBackLoop();

    void     set_main_loop(GMainLoop*);

private :
    GMainLoop*      m_mainloop_;

signals:
    void            signal_exit_main_loop();

public slots:
    void            slot_do_work();
};

#endif // VOIPSERVER_RINGBACK_LOOP_H
